

!define NAMEPG 		"Postgre@Etersoft"
!define NAMEFILE	"postgre-etersoft"
!define SMALNAMEPG	"pgsql"


!define VERMAJOR "9.0"
!define VERMINOR "1"

!define VERPG "9.0"
!define sufix ""
!define REVPG "2"

!define SSLPATH "\var\ftp\pvt\Windows\4makepg\nsis\OpenSSL-Win32"
!define LIBMINGWPATHFROM "\usr\i586-pc-mingw32\sys-root\mingw\bin"
!define MSVCRPATHFROM "\var\ftp\pvt\Windows\4makepg\nsis\msvcr\90"
!define PGHEADPATHFROM ".."
!define PGADMINPATHFROM "\var\ftp\pvt\Windows\4makepg\nsis\pgAdmin III\1.12"
!define LICENSEPATH "AdditionalFiles\lic"
!define TIMEZONEFROM "\var\ftp\pvt\Windows\4makepg\nsis\timezone"
!define ICOPATH "AdditionalFiles\ico"  


!define DEFAULTPORT	"5432"
!define DEFAULTENCODING	"UTF-8"
!define DEFAULTLOCALE	"Russia, Russia"


!define PRODUCTCODE 	"E91D9298-2641-4F1A-9C20-6928B142C9DC"


!define SERVICEDISPLAYNAME ${SMALNAMEPG}-${VERPG}-${sufix}${REVPG}
!define VALIDATEPASSWORDSTRING "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+|`\\=-}{$\":?><][';/.,"
