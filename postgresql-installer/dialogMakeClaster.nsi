!define CUSTMCL_INI "$PLUGINSDIR\custommakecluster.ini"
!define LISTENCODING       "EUC_JP|EUC_CN|EUC_KR|EUC_TW|JOHAB|LATIN1|LATIN2|LATIN3|LATIN4|LATIN5|LATIN6|LATIN7|LATIN8|LATIN9|WIN1256|WIN1258|WIN866|WIN874|KOI8|WIN1251|WIN1252|ISO_8859_5|ISO_8859_6|ISO_8859_7|ISO_8859_8|WIN1250|WIN1253|WIN1254|WIN1255|WIN1257|SJIS|BIG5|GBK|UHC|GB18030|UTF8"


Function OnInitMakeCluster

   var /GLOBAL CUSTMCL_INI

   GetTempFileName $1
   StrCpy $CUSTMCL_INI $1
   Pop $1

   localelist::GetStringSystemLocales
   Pop $0

   Call WriteIniMakeCluster
   WriteIniStr '$CUSTMCL_INI' 'Field 3' 'State' '${DEFAULTPORT}'
   WriteIniStr '$CUSTMCL_INI' 'Field 7' 'ListItems' '$0'
   WriteIniStr '$CUSTMCL_INI' 'Field 7' 'State' 'Russian_Russia'
   WriteIniStr '$CUSTMCL_INI' 'Field 10' 'State' 'UTF8'
   WriteIniStr '$CUSTMCL_INI' 'Field 14' 'State' 'postgres'

FunctionEnd

Function CustomMakeClusterCreate
   InstallOptions::Dialog '$CUSTMCL_INI'
FunctionEnd



Function SetParamMakeCluster

   var /GLOBAL ISINIT
   var /GLOBAL PORT
   var /GLOBAL ISONLYLOCALHOST
   var /GLOBAL LOCALE
   var /GLOBAL SERVERENCODING
   var /GLOBAL CLIENTENCODING
   var /GLOBAL SUPASSWORD
   var /GLOBAL SUNAME

   Push $8
   Push $7
   Push $6
   Push $5
   Push $4
   Push $3
   Push $2
   Push $1
   Push $0

   ReadIniStr $8 '$CUSTMCL_INI' 'Field 1' 'State'
   ReadIniStr $7 '$CUSTMCL_INI' 'Field 3' 'State' 
   ReadIniStr $6 '$CUSTMCL_INI' 'Field 5' 'State'
   ReadIniStr $5 '$CUSTMCL_INI' 'Field 7' 'State'
   ReadIniStr $4 '$CUSTMCL_INI' 'Field 10' 'State'
   ReadIniStr $3 '$CUSTMCL_INI' 'Field 12' 'State'
   ReadIniStr $2 '$CUSTMCL_INI' 'Field 14' 'State'
   ReadIniStr $1 '$CUSTMCL_INI' 'Field 16' 'State'
   ReadIniStr $0 '$CUSTMCL_INI' 'Field 18' 'State'

   StrCpy $ISINIT $8
   StrCmp $ISINIT "0" ENDSETPARAMMAKECLUSTER 

   StrCpy $PORT $7
   StrCpy $ISONLYLOCALHOST $6
   StrCpy $LOCALE $5
   StrCpy $SERVERENCODING $4
   StrCpy $CLIENTENCODING $3
   StrCpy $SUNAME $2
   StrCpy $SUPASSWORD $1
	
CHECKSAMEPASSWORD:
   StrCmp $0 $1 0 ERRORCMPPASSWORD
   goto CHECKNULLSTRING
ERRORCMPPASSWORD:
   MessageBox MB_OK|MB_ICONINFORMATION \
   "��������� ������ �� ��������� " IDOK
   abort

CHECKNULLSTRING: 
   StrCmp $SUPASSWORD "" ERRORNULLSTRING
   goto CHECKVALIDATESTRING
ERRORNULLSTRING:
   MessageBox MB_OK|MB_ICONINFORMATION \
   "������� ������" IDOK
   abort

CHECKVALIDATESTRING:
   Push $SUPASSWORD
   Push ${VALIDATEPASSWORDSTRING}
   Call Validate
   Pop $0
   StrCmp $0 "0" ERRORVALIDATESTRING
   goto ENDSETPARAMMAKECLUSTER
ERRORVALIDATESTRING:
   MessageBox MB_OK|MB_ICONINFORMATION \
   "������������ ������� � ������" IDOK
   abort

ENDSETPARAMMAKECLUSTER:
							
FunctionEnd


Function CustomMakeClusterLeave

   call SetParamMakeCluster

   var /GLOBAL PGDATA
   var /GLOBAL XLOG	
   var /GLOBAL PATHINITDB

   StrCpy $PGDATA "$INSTDIR\Data"
   StrCpy $XLOG "$INSTDIR\pg_xlog"
   StrCpy $PATHINITDB "$INSTDIR\bin"

FunctionEnd


Function CustomMakeClusterMake
   StrCmp $ISINIT "1" INIT
   goto ENDCUSTOMMAKECLUSTERMAKE

INIT:
   var /GLOBAL PATHFILEPW
   GetTempFileName $0
   ;StrCpy $PATHFILEPW $0
   StrCpy $PATHFILEPW "$INSTDIR\.tpmpwd.tmp"
   
   ExecCmd::exec  /NOUNLOAD /ASYNC \
   "echo $SUPASSWORD> $\"$PATHFILEPW$\"" ""
   Pop $0
   ExecCmd::wait $0

   var /GLOBAL COMMAND
 


   StrCpy $COMMAND "$\"$PATHINITDB\initdb.exe$\" -U postgres --locale=$\"$LOCALE$\" --pwfile $\"$PATHFILEPW$\" --encoding $SERVERENCODING --xlogdir=$\"$XLOG$\" --pgdata $\"$PGDATA$\""
   RunAs::RunAsW "$SERVICEACCOUNT" "" "$SERVICEPASSWORD" "$COMMAND" "$INSTDIR\pgstartup.log"
   Pop $0




   ExecCmd::exec  /NOUNLOAD /ASYNC \
      "echo 0000000000000000000000000000000000000000000000000000000000000000000000000000 > $\"$PATHFILEPW$\""

   Pop $0
   ExecDos::wait $0
   ExecCmd::exec  /NOUNLOAD /ASYNC \
      "del $\"$PATHFILEPW$\""
   Pop $0
   ExecDos::wait $0
   
   
   delete $\"$PATHFILEPW$\"
ENDCUSTOMMAKECLUSTERMAKE:

FunctionEnd





Function WriteIniMakeCluster
WriteIniStr '$CUSTMCL_INI' 'Settings' 'NumFields' '18'

WriteIniStr 	'$CUSTMCL_INI'	'Field 1'	'Type' 		'CheckBox'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 1'	'Left' 		'0'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 1'	'Top' 		'0'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 1'	'Right' 	'250'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 1'	'Bottom'	'10'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 1'	'Text'		'������������ ������� ��� ������'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 1'	'State'		'1'

;WriteIniStr 	'$CUSTMCL_INI'	'Field 2'	'Type' 		'Label'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 2'	'Left' 		'3'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 2'	'Top' 		'14'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 2'	'Right' 	'25'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 2'	'Bottom'	'23'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 2'	'Text'		'����'

;WriteIniStr 	'$CUSTMCL_INI'	'Field 3'	'Type' 		'Text'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 3'	'Left' 		'60'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 3'	'Top' 		'12'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 3'	'Right' 	'90'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 3'	'Bottom'	'25'

;WriteIniStr 	'$CUSTMCL_INI'	'Field 4'	'Type' 		'Label'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 4'	'Left' 		'3'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 4'	'Top' 		'30'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 4'	'Right' 	'30'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 4'	'Bottom'	'40'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 4'	'Text'		'������'

;WriteIniStr 	'$CUSTMCL_INI'	'Field 5'	'Type' 		'CheckBox'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 5'	'Left' 		'60'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 5'	'Top' 		'30'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 5'	'Right' 	'250'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 5'	'Bottom'	'40'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 5'	'Text'		'����������� ���������� � ����� ip'

WriteIniStr 	'$CUSTMCL_INI'	'Field 6'	'Type' 		'Label'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 6'	'Left' 		'43'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 6'	'Top' 		'12'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 6'	'Right' 	'120'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 6'	'Bottom'	'21'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 6'	'Text'		'������'

WriteIniStr 	'$CUSTMCL_INI'	'Field 7'	'Type' 		'Combobox'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 7'	'Left' 		'130'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 7'	'Top' 		'10'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 7'	'Right' 	'220'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 7'	'Bottom'	'90'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 7'	'ListItems'	'Russian_Russia.1251 |Azeri_Azerbaijan |C'

WriteIniStr 	'$CUSTMCL_INI'	'Field 8'	'Type' 		'Label'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 8'	'Left' 		'43'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 8'	'Top' 		'32'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 8'	'Right' 	'120'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 8'	'Bottom'	'41'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 8'	'Text'		'��������� �������'

;WriteIniStr 	'$CUSTMCL_INI'	'Field 9'	'Type' 		'Label'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 9'	'Left' 		'95'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 9'	'Top' 		'43'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 9'	'Right' 	'127'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 9'	'Bottom'	'49'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 9'	'Text'		'(�������)'

WriteIniStr 	'$CUSTMCL_INI'	'Field 10'	'Type' 		'Combobox'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 10'	'Left' 		'130'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 10'	'Top' 		'30'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 10'	'Right' 	'220'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 10'	'Bottom'	'110'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 10'	'ListItems'	'${LISTENCODING}'

;WriteIniStr 	'$CUSTMCL_INI'	'Field 11'	'Type' 		'Label'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 11'	'Left' 		'144'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 11'	'Top' 		'62'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 11'	'Right' 	'177'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 11'	'Bottom'	'77'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 11'	'Text'		'(�������)'

;WriteIniStr 	'$CUSTMCL_INI'	'Field 12'	'Type' 		'Combobox'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 12'	'Left' 		'180'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 12'	'Top' 		'59'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 12'	'Right' 	'260'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 12'	'Bottom'	'153'
;WriteIniStr 	'$CUSTMCL_INI' 	'Field 12'	'ListItems'	''

WriteIniStr 	'$CUSTMCL_INI'	'Field 13'	'Type' 		'Label'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 13'	'Left' 		'43'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 13'	'Top' 		'62'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 13'	'Right' 	'120'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 13'	'Bottom'	'71'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 13'	'Text'		'��� ����������'

WriteIniStr 	'$CUSTMCL_INI'	'Field 14'	'Type' 		'Text'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 14'	'Left' 		'130'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 14'	'Top' 		'60'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 14'	'Right' 	'220'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 14'	'Bottom'	'73'

WriteIniStr 	'$CUSTMCL_INI'	'Field 15'	'Type' 		'Label'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 15'	'Left' 		'43'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 15'	'Top' 		'82'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 15'	'Right' 	'120'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 15'	'Bottom'	'92'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 15'	'Text'		'������'

WriteIniStr 	'$CUSTMCL_INI'	'Field 16'	'Type' 		'Password'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 16'	'Left' 		'130'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 16'	'Top' 		'80'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 16'	'Right' 	'220'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 16'	'Bottom'	'93'

WriteIniStr 	'$CUSTMCL_INI'	'Field 17'	'Type' 		'Label'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 17'	'Left' 		'43'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 17'	'Top' 		'102'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 17'	'Right' 	'120'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 17'	'Bottom'	'112'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 17'	'Text'		'�������������'

WriteIniStr 	'$CUSTMCL_INI'	'Field 18'	'Type' 		'Password'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 18'	'Left' 		'130'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 18'	'Top' 		'100'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 18'	'Right' 	'220'
WriteIniStr 	'$CUSTMCL_INI' 	'Field 18'	'Bottom'	'113'
FunctionEnd
