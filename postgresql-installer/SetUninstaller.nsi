!include "AdditionalFunc.nsi"

Function CustomSetUninstaller 
   ${WriteUninstaller} "$INSTDIR\uninst.exe"
FunctionEnd

Section -closelogfile
 FileClose $UninstLog
 SetFileAttributes "$INSTDIR\${UninstLog}" READONLY|SYSTEM|HIDDEN
SectionEnd

Function un.GetNameAccount
	
   ClearErrors
   ReadRegStr $1 ${MAINKEY} ${REGSERVICE} "Service Account"
   IfErrors LABEL_ERROR
   
   var /GLOBAL ACCOUNT_SERVER
   StrCpy $ACCOUNT_SERVER $1

   var /GLOBAL SLASH_STRING
   StrCpy $SLASH_STRING "\"

   ${StrStr} $0 $ACCOUNT_SERVER $SLASH_STRING

   Push $0 
   Push $ACCOUNT_SERVER
   Call un.Slice
   Pop $R0 

   var /GLOBAL SERVER
   StrCpy $SERVER $R0 


   Push $SLASH_STRING ; ...to cut from
   Push $0
   Call un.Slice
   Pop $R0 

   var /GLOBAL ACCOUNT
   StrCpy $ACCOUNT $R0 

LABEL_ERROR:
FunctionEnd




Function un.GetStartParam
  
   call un.GetNameAccount
   
FunctionEnd






Function un.DeleteFileAll

; Can't uninstall if uninstall log is missing!
 IfFileExists "$INSTDIR\${UninstLog}" +3
  MessageBox MB_OK|MB_ICONSTOP "$(UninstLogMissing)"
   Abort
 
 Push $R0
 Push $R1
 Push $R2
 SetFileAttributes "$INSTDIR\${UninstLog}" NORMAL
 FileOpen $UninstLog "$INSTDIR\${UninstLog}" r
 StrCpy $R1 -1
 
 GetLineCount:
  ClearErrors
  FileRead $UninstLog $R0
  IntOp $R1 $R1 + 1
  StrCpy $R0 $R0 -2
  Push $R0   
  IfErrors 0 GetLineCount
 
 Pop $R0
 
 LoopRead:
  StrCmp $R1 0 LoopDone
  Pop $R0
 
  IfFileExists "$R0\*.*" 0 +3
   RMDir $R0  #is dir
  Goto +3
  IfFileExists $R0 0 +2
   Delete $R0 #is file
 
  IntOp $R1 $R1 - 1
  Goto LoopRead
 LoopDone:
 FileClose $UninstLog
 Delete "$INSTDIR\${UninstLog}"
 RMDir "$INSTDIR"
 Pop $R2
 Pop $R1
 Pop $R0

FunctionEnd



Section "Uninstall"

   Call un.GetStartParam

   UserMgr::DeleteAccountEx $ACCOUNT $SERVER

   nsSCM::Stop ${SERVICEDISPLAYNAME}
   nsSCM::Remove ${SERVICEDISPLAYNAME}

   
   Call un.DeleteFileAll

   Delete "${BINPATHTO}\initdb.log"
   Delete "${BINPATHTO}\postgresmanagement.bat"
   RMDir "${BINPATHTO}"
   RMDir "${LIBPATHTO}"
   RMDir "${SHAREPATHTO}"
   RMDir "${PGADMINPATHTO}\docs\en_US"
   RMDir "${PGADMINPATHTO}\docs"
   RMDir "${PGADMINPATHTO}"
   
   DeleteRegKey ${MAINKEY} ${REGSERVICE}
   DeleteRegKey ${MAINKEY} ${REGINTPG}
   DeleteRegKey HKLM  "${REGUNINSTALLER}"



   RMDir /r "$SMPROGRAMS\${ICONS_GROUP}" #����� � ����"

   Delete "$INSTDIR\uninst.exe"
   
   SetRebootFlag true

SectionEnd
