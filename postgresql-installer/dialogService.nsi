
;Name "${APP_NAME}"
;OutFile "${APP_NAME}.exe"
;ShowInstDetails show
;LicenseData "License.txt"
!define CUSTSER_INI "$PLUGINSDIR\customservice.ini"


Function OnInitService

   var /GLOBAL NAMELOCALSERVER
   ReadRegStr $1 HKLM "System\CurrentControlSet\Control\ComputerName\ActiveComputerName" "ComputerName"
   StrCmp $1 "" WIN9X
   Goto DONE
WIN9X:
   ReadRegStr $0 HKLM "System\CurrentControlSet\Control\ComputerName\ComputerName" "ComputerName"
DONE:

   var /GLOBAL CUSTSER_INI
   InitPluginsDir
   GetTempFileName $0

   StrCpy $CUSTSER_INI $0
   Pop $0
   Call WriteServiceIni
	 
   StrCpy $NAMELOCALSERVER  $1
   WriteIniStr '$CUSTSER_INI' 'Field 3' 'State' '$NAMELOCALSERVER'
   Pop $1
FunctionEnd
 

 
Function CustomServiceCreate
   InstallOptions::Dialog '$CUSTSER_INI'
FunctionEnd
 
 


Function CustomServiceLeave


   var /GLOBAL SERVICENAME
   var /GLOBAL SERVICEACCOUNT
   var /GLOBAL SERVICEDOMAIN
   var /GLOBAL SERVICEPASSWORD

   Push $4
   Push $3
   Push $2
   Push $1
   Push $0

   ReadIniStr $4 '$CUSTSER_INI' 'Field 1' 'State' 
   ReadIniStr $3 '$CUSTSER_INI' 'Field 2' 'State'
   ReadIniStr $2 '$CUSTSER_INI' 'Field 3' 'State'
   ReadIniStr $1 '$CUSTSER_INI' 'Field 4' 'State'
   ReadIniStr $0 '$CUSTSER_INI' 'Field 5' 'State'
        
   StrCpy $SERVICENAME $4
   StrCpy $SERVICEACCOUNT $3
   StrCpy $SERVICEDOMAIN $2
   StrCpy $SERVICEPASSWORD $1
   

CHECKSAMEPASSWORD:
   StrCmp $0 $1 0 ERRORCMPPASSWORD
   goto CHECKNULLSTRING
ERRORCMPPASSWORD:
   MessageBox MB_OK|MB_ICONINFORMATION \
   "��������� ������ �� ��������� " IDOK
   abort

CHECKNULLSTRING: 
   StrCmp $SERVICEPASSWORD "" ERRORNULLSTRING
   goto CHECKVALIDATESTRING
ERRORNULLSTRING:
   MessageBox MB_OK|MB_ICONINFORMATION \
   "������� ������" IDOK
   abort

CHECKVALIDATESTRING:
   Push $SERVICEPASSWORD
   Push ${VALIDATEPASSWORDSTRING}
   Call Validate
   Pop $0
   StrCmp $0 "0" ERRORVALIDATESTRING
   goto ENDSETPARAMMAKECLUSTER
ERRORVALIDATESTRING:
   MessageBox MB_OK|MB_ICONINFORMATION \
   "������������ ������� � ������" IDOK
   abort

ENDSETPARAMMAKECLUSTER:



DONE:
   call CheckUserPostgres
   StrCmp $0 "ERROR" 0 ENDCUSTOMSERVICELEAVE
   abort

ENDCUSTOMSERVICELEAVE:
   Pop $0
   Pop $1
   Pop $2
   Pop $3
   Pop $4
FunctionEnd
 
Function WriteServiceIni
WriteIniStr '$CUSTSER_INI' 'Settings' 'NumFields' '10'

 
WriteIniStr '$CUSTSER_INI' 'Field 1' 'Type' 'Text'
WriteIniStr '$CUSTSER_INI' 'Field 1' 'Left' '70'
WriteIniStr '$CUSTSER_INI' 'Field 1' 'Top' '5'
WriteIniStr '$CUSTSER_INI' 'Field 1' 'Right' '-20'
WriteIniStr '$CUSTSER_INI' 'Field 1' 'Bottom' '20'
WriteIniStr '$CUSTSER_INI' 'Field 1' 'State' 'PostgreSQL Database Server ${VERPG}-eter${REVPG}'
WriteIniStr '$CUSTSER_INI' 'Field 1' 'Minlen' '2'
WriteIniStr '$CUSTSER_INI' 'Field 1' 'ValidateText' ''
 
WriteIniStr '$CUSTSER_INI' 'Field 2' 'Type' 'Text'
WriteIniStr '$CUSTSER_INI' 'Field 2' 'Left' '70'
WriteIniStr '$CUSTSER_INI' 'Field 2' 'Top' '30'
WriteIniStr '$CUSTSER_INI' 'Field 2' 'Right' '-20'
WriteIniStr '$CUSTSER_INI' 'Field 2' 'Bottom' '45'
WriteIniStr '$CUSTSER_INI' 'Field 2' 'State' 'postgres'
WriteIniStr '$CUSTSER_INI' 'Field 2' 'Minlen' '2'
WriteIniStr '$CUSTSER_INI' 'Field 2' 'ValidateText' ''

WriteIniStr '$CUSTSER_INI' 'Field 3' 'Type' 'Text'
WriteIniStr '$CUSTSER_INI' 'Field 3' 'Left' '70'
WriteIniStr '$CUSTSER_INI' 'Field 3' 'Top' '55'
WriteIniStr '$CUSTSER_INI' 'Field 3' 'Right' '-20'
WriteIniStr '$CUSTSER_INI' 'Field 3' 'Bottom' '70'
WriteIniStr '$CUSTSER_INI' 'Field 3' 'State' ''
WriteIniStr '$CUSTSER_INI' 'Field 3' 'Minlen' '2'
WriteIniStr '$CUSTSER_INI' 'Field 3' 'ValidateText' ''

WriteIniStr '$CUSTSER_INI' 'Field 4' 'Type' 'Password'
WriteIniStr '$CUSTSER_INI' 'Field 4' 'Left' '70'
WriteIniStr '$CUSTSER_INI' 'Field 4' 'Top' '80'
WriteIniStr '$CUSTSER_INI' 'Field 4' 'Right' '-20'
WriteIniStr '$CUSTSER_INI' 'Field 4' 'Bottom' '95'
WriteIniStr '$CUSTSER_INI' 'Field 4' 'State' ''
WriteIniStr '$CUSTSER_INI' 'Field 4' 'ValidateText' ''

WriteIniStr '$CUSTSER_INI' 'Field 5' 'Type' 'Password'
WriteIniStr '$CUSTSER_INI' 'Field 5' 'Left' '70'
WriteIniStr '$CUSTSER_INI' 'Field 5' 'Top' '105'
WriteIniStr '$CUSTSER_INI' 'Field 5' 'Right' '-20'
WriteIniStr '$CUSTSER_INI' 'Field 5' 'Bottom' '120'
WriteIniStr '$CUSTSER_INI' 'Field 5' 'State' ''
WriteIniStr '$CUSTSER_INI' 'Field 5' 'ValidateText' ''

WriteIniStr '$CUSTSER_INI' 'Field 6' 'Type' 'Label'
WriteIniStr '$CUSTSER_INI' 'Field 6' 'Left' '10'
WriteIniStr '$CUSTSER_INI' 'Field 6' 'Top' '5'
WriteIniStr '$CUSTSER_INI' 'Field 6' 'Right' '-25'
WriteIniStr '$CUSTSER_INI' 'Field 6' 'Bottom' '68'
WriteIniStr '$CUSTSER_INI' 'Field 6' 'Text' \
'��� �������'

WriteIniStr '$CUSTSER_INI' 'Field 7' 'Type' 'Label'
WriteIniStr '$CUSTSER_INI' 'Field 7' 'Left' '10'
WriteIniStr '$CUSTSER_INI' 'Field 7' 'Top' '30'
WriteIniStr '$CUSTSER_INI' 'Field 7' 'Right' '-25'
WriteIniStr '$CUSTSER_INI' 'Field 7' 'Bottom' '45'
WriteIniStr '$CUSTSER_INI' 'Field 7' 'Text' \
'������� ������'

WriteIniStr '$CUSTSER_INI' 'Field 8' 'Type' 'Label'
WriteIniStr '$CUSTSER_INI' 'Field 8' 'Left' '10'
WriteIniStr '$CUSTSER_INI' 'Field 8' 'Top' '55'
WriteIniStr '$CUSTSER_INI' 'Field 8' 'Right' '-25'
WriteIniStr '$CUSTSER_INI' 'Field 8' 'Bottom' '70'
WriteIniStr '$CUSTSER_INI' 'Field 8' 'Text' \
'�����'

WriteIniStr '$CUSTSER_INI' 'Field 9' 'Type' 'Label'
WriteIniStr '$CUSTSER_INI' 'Field 9' 'Left' '10'
WriteIniStr '$CUSTSER_INI' 'Field 9' 'Top' '80'
WriteIniStr '$CUSTSER_INI' 'Field 9' 'Right' '-25'
WriteIniStr '$CUSTSER_INI' 'Field 9' 'Bottom' '95'
WriteIniStr '$CUSTSER_INI' 'Field 9' 'Text' \
'������'

WriteIniStr '$CUSTSER_INI' 'Field 10' 'Type' 'Label'
WriteIniStr '$CUSTSER_INI' 'Field 10' 'Left' '10'
WriteIniStr '$CUSTSER_INI' 'Field 10' 'Top' '105'
WriteIniStr '$CUSTSER_INI' 'Field 10' 'Right' '-25'
WriteIniStr '$CUSTSER_INI' 'Field 10' 'Bottom' '120'
WriteIniStr '$CUSTSER_INI' 'Field 10' 'Text' \
'�������������'

Functionend
