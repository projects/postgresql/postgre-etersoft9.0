!define ICONS_GROUP "${NAMEPG} ${VERPG}-${sufix}${REVPG}"

Function WriteToFile
 Exch $0 ;file to write to
 Exch
 Exch $1 ;text to write
 
  FileOpen $0 $0 a #open file
   FileSeek $0 0 END #go to end
   FileWrite $0 $1 #write to file
  FileClose $0
 
 Pop $1
 Pop $0
FunctionEnd


Function createbatmanage

Push \
"@echo off$\r$\n \
rem *****************************************************$\r$\n \
rem Manage postgres service$\r$\n \
rem *****************************************************$\r$\n \
set NAMESERVICE=${SERVICEDISPLAYNAME}$\r$\n \ 
set ACTION=%1$\r$\n \
if $\"%ACTION%$\"==$\"start$\" ($\r$\n \
@echo start postgres ...$\r$\n \
sc start %NAMESERVICE%$\r$\n \
) else if $\"%ACTION%$\"==$\"stop$\" ($\r$\n \
@echo stop postgres ...$\r$\n \
sc stop %NAMESERVICE%$\r$\n \
) else if $\"%ACTION%$\"==$\"restart$\" ($\r$\n \
@echo stop postgres ...$\r$\n \
sc stop %NAMESERVICE%$\r$\n \
@echo start postgres ...$\r$\n \
sc start %NAMESERVICE%$\r$\n \
) else if $\"%ACTION%$\"==$\"status$\" ($\r$\n \
sc query %NAMESERVICE%$\r$\n \
) else ($\r$\n \
@echo postgresmanagement.bat:  {start^|stop^|restart^|status}$\r$\n \
)$\r$\n"

Push "${BINPATHTO}\postgresmanagement.bat" ;file to write to 
Call WriteToFile

FunctionEnd

Function createstartshortcut

   call createbatmanage
   CreateDirectory "$SMPROGRAMS\${ICONS_GROUP}"
   CreateDirectory "$SMPROGRAMS\${ICONS_GROUP}\���������������� �����"
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\���������������� �����\������������� pg_hba.conf.lnk" "notepad"  "$PGDATA\pg_hba.conf"
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\���������������� �����\������������� pg_ident.conf.lnk" "notepad"  "$PGDATA\pg_ident.conf"
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\���������������� �����\������������� postgresql.conf.lnk" "notepad" "$PGDATA\postgresql.conf"
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\psql.lnk" "${BINPATHTO}\psql.exe" "-U $SUNAME"
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\PgAdmin3.lnk" "${BINPATHTO}\PgAdmin3.exe"
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\��������� ������.lnk" "${BINPATHTO}\postgresmanagement.bat" "start"  
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\���������� ������.lnk" "${BINPATHTO}\postgresmanagement.bat" "stop" 
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\������������� ������.lnk" "${BINPATHTO}\postgresmanagement.bat" "restart" 
   CreateShortCut "$SMPROGRAMS\${ICONS_GROUP}\������� Postgres.lnk" "$INSTDIR\uninst.exe"
   
   



FunctionEnd
