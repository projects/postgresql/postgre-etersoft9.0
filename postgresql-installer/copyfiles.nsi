!include LogicLib.nsh
!define CONTRIBPATHFROM "${PGHEADPATHFROM}\contrib"
!define BINPAHFROM 	"${PGHEADPATHFROM}\src\bin"
!define BACKENDPATHFROM "${PGHEADPATHFROM}\src\backend"
!define PLPATHFROM 	"${PGHEADPATHFROM}\src\pl"
!define INTERFACESFROM 	"${PGHEADPATHFROM}\src\interfaces"

!define TZNAMESFROM 	"${PGHEADPATHFROM}\src\timezone\tznames\"



!define SCRIPTSPATHFROM "${BINPAHFROM}\scripts"
!define HTMLPATHFROM 	"${PGADMINPATHFROM}\Docs"
!define MOPATHFROM 	"${PGADMINPATHFROM}\i18n"


!define BINPATHTO 	"$INSTDIR\bin"
!define LIBPATHTO 	"$INSTDIR\lib"
!define SHAREPATHTO 	"$INSTDIR\share"
!define SQLPATHTO 	"$INSTDIR\share\contrib"
!define PGADMINPATHTO 	"$INSTDIR\pgAdmin III"
!define HTMLPATHTO 	"${PGADMINPATHTO}\docs"
!define MOPATHTO 	"${PGADMINPATHTO}\i18n"

Section "serverbin" SEC01

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "clusterdb.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "createdb.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "createlang.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "createuser.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "dropdb.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "droplang.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "dropuser.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\initdb\" "initdb.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\pg_config\" "pg_config.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\pg_controldata\" "pg_controldata.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\pg_ctl\" "pg_ctl.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\pg_dump\" "pg_dump.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\pg_dump\" "pg_dumpall.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\pg_resetxlog\" "pg_resetxlog.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\pg_dump\" "pg_restore.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_standby\" "pg_standby.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BACKENDPATHFROM}\" "postgres.exe"

   ;WARNING �� ������� ��� ���. �� ����� �� ���������������
   ;${SetOutPath} "${BINPATHTO}\"
   ;${File} "postgres.lib"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "reindexdb.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SCRIPTSPATHFROM}\" "vacuumdb.exe"

   ;WARNING ������ ���������������. ������ ���� � ����������.
   ;${SetOutPath} "${BINPATHTO}\"
   ;${File} "pgperm.bat"

SectionEnd

Section "serverlib" SEC02
   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${BINPAHFROM}\pgevent\" "pgevent.dll"

   ${SetOutPath} "${LIBPATHTO}\"

   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\ascii_and_mic\" "ascii_and_mic.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\cyrillic_and_mic\" "cyrillic_and_mic.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\euc_cn_and_mic\" "euc_cn_and_mic.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\euc2004_sjis2004\" "euc2004_sjis2004.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\euc_jp_and_sjis\" "euc_jp_and_sjis.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\euc_kr_and_mic\" "euc_kr_and_mic.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\euc_tw_and_big5\" "euc_tw_and_big5.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\latin2_and_win1250\" "latin2_and_win1250.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\latin_and_mic\" "latin_and_mic.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_ascii\" "utf8_and_ascii.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_big5\" "utf8_and_big5.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_cyrillic\" "utf8_and_cyrillic.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_euc_cn\" "utf8_and_euc_cn.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_euc2004\" "utf8_and_euc2004.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_euc_jp\" "utf8_and_euc_jp.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_euc_kr\" "utf8_and_euc_kr.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_euc_tw\" "utf8_and_euc_tw.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_gb18030\" "utf8_and_gb18030.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_gbk\" "utf8_and_gbk.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_iso8859\" "utf8_and_iso8859.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_iso8859_1\" "utf8_and_iso8859_1.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_johab\" "utf8_and_johab.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_sjis2004\" "utf8_and_sjis2004.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_sjis\" "utf8_and_sjis.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_uhc\" "utf8_and_uhc.dll"
   ${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\utf8_and_win\" "utf8_and_win.dll"


   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${PLPATHFROM}\plpgsql\src\" "plpgsql.dll"

; WARNING �� ��������� ����
;   ${SetOutPath} "${LIBPATHTO}\"				
;   ${File} "${PLPATHFROM}\plperl\src\" "plperl.dll"

; WARNING �� ��������� ����
;   ${SetOutPath} "${LIBPATHTO}\"	
;   ${File} "${PLPATHFROM}\pltcl\src\" "pltcl.dll"

; WARNING �� ��������� ����
;   ${SetOutPath} "${LIBPATHTO}\"	
;   ${File} "${PLPATHFROM}\plpython\src\" "plpython.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${BACKENDPATHFROM}\snowball\" "dict_snowball.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\adminpack\" "adminpack.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\seltapgaddon\" "seltapgaddon.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\auto_explain\" "auto_explain.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\btree_gin\" "btree_gin.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\btree_gist\" "btree_gist.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\chkpass\" "chkpass.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\citext\" "citext.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\cube\" "cube.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\dblink\" "dblink.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\earthdistance\" "earthdistance.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\fuzzystrmatch\" "fuzzystrmatch.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\hstore\" "hstore.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\intarray\" "_int.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\isn\" "isn.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\lo\" "lo.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\ltree\" "ltree.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pageinspect\" "pageinspect.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_buffercache\" "pg_buffercache.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_freespacemap\" "pg_freespacemap.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_stat_statements\" "pg_stat_statements.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_trgm\" "pg_trgm.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgcrypto\" "pgcrypto.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgrowlocks\" "pgrowlocks.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgstattuple\" "pgstattuple.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\seg\" "seg.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "autoinc.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "insert_username.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "moddatetime.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "refint.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "timetravel.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\mchar\" "mchar.dll"

   ;${SetOutPath} "${LIBPATHTO}\"
   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\mchar\icu\bin\" "icudt42.dll"
   ${File} "${CONTRIBPATHFROM}\mchar\icu\bin\" "icuin42.dll"
   ${File} "${CONTRIBPATHFROM}\mchar\icu\bin\" "icuio42.dll"
   ${File} "${CONTRIBPATHFROM}\mchar\icu\bin\" "icuuc42.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\fasttrun\" "fasttrun.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\fulleq\" "fulleq.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\tsearch2\" "tsearch2.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\oid2name\" "oid2name.exe"



; WARNING �� ���������
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${CONTRIBPATHFROM}\sslinfo\" "sslinfo.dll"

   ${SetOutPath} "${LIBPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\tablefunc\" "tablefunc.dll"

; WARNING �� ���������
;   ${SetOutPath} "${LIBPATHTO}\" 
;   ${File} "${CONTRIBPATHFROM}\uuid-ossp\" "uuid-ossp.dll"

; WARNING �� ���������
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${CONTRIBPATHFROM}\xml2\" "pgxml.dll"



SectionEnd




Section "serversql" SEC03


   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\adminpack\" "adminpack.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\seltapgaddon\" "seltapgaddon.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\seltapgaddon\" "uninstall_seltapgaddon.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\adminpack\" "uninstall_adminpack.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\btree_gin\" "btree_gin.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\btree_gin\" "uninstall_btree_gin.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\btree_gist\" "btree_gist.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\btree_gist\" "uninstall_btree_gist.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\chkpass\" "chkpass.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\chkpass\" "uninstall_chkpass.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\citext\" "citext.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\citext\" "uninstall_citext.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\cube\" "cube.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\cube\" "uninstall_cube.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\dblink\" "dblink.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\dblink\" "uninstall_dblink.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\earthdistance\" "earthdistance.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\earthdistance\" "uninstall_earthdistance.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\fuzzystrmatch\" "fuzzystrmatch.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\fuzzystrmatch\" "uninstall_fuzzystrmatch.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\hstore\" "hstore.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\hstore\" "uninstall_hstore.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\intagg\" "int_aggregate.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\intagg\" "uninstall_int_aggregate.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\intarray\" "_int.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\intarray\" "uninstall__int.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\isn\" "isn.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\isn\" "uninstall_isn.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\lo\" "lo.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\lo\" "uninstall_lo.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\ltree\" "ltree.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\ltree\" "uninstall_ltree.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pageinspect\" "pageinspect.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pageinspect\" "uninstall_pageinspect.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_buffercache\" "pg_buffercache.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_buffercache\" "uninstall_pg_buffercache.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_freespacemap\" "pg_freespacemap.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_freespacemap\" "uninstall_pg_freespacemap.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_stat_statements\" "pg_stat_statements.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_stat_statements\" "uninstall_pg_stat_statements.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_trgm\" "pg_trgm.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pg_trgm\" "uninstall_pg_trgm.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgcrypto\" "pgcrypto.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgrowlocks\" "pgrowlocks.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgrowlocks\" "uninstall_pgrowlocks.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgstattuple\" "pgstattuple.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgstattuple\" "uninstall_pgstattuple.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\seg\" "seg.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\seg\" "uninstall_seg.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "autoinc.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "insert_username.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "moddatetime.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "refint.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\spi\" "timetravel.sql"

   ;���� ��� SSL
   ;${SetOutPath} "${SQLPATHTO}\"
   ;${File} "${CONTRIBPATHFROM}\sslinfo\" "sslinfo.sql"

   ;${SetOutPath} "${SQLPATHTO}\"
   ;${File} "${CONTRIBPATHFROM}\sslinfo\" "uninstall_sslinfo.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\tablefunc\" "tablefunc.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\tablefunc\" "uninstall_tablefunc.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\uuid-ossp\" "uuid-ossp.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\uuid-ossp\" "uninstall_uuid-ossp.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\xml2\" "pgxml.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\xml2\" "uninstall_pgxml.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\mchar\" "mchar.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\mchar\" "uninstall_mchar.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\fasttrun\" "fasttrun.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\fulleq\" "fulleq.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\intagg\" "int_aggregate.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\intagg\" "uninstall_int_aggregate.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\tsearch2\" "tsearch2.sql"

   ${SetOutPath} "${SQLPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\tsearch2\" "uninstall_tsearch2.sql"

 
; WARNING pldebugger �� ���������
;   ${SetOutPath} "${SQLPATHTO}\"                       ;
;   ${File} "${CONTRIBPATHFROM}\pldebugger\" "pldbgapi.sql"

SectionEnd


Section "serverhtml" SEC04
   ${SetOutPath} "${HTMLPATHTO}\cs_CZ\Hints\"
   ${File} "${HTMLPATHFROM}\cs_CZ\Hints\" "*.html"

   ${SetOutPath} "${HTMLPATHTO}\cs_CZ\"
   ${File} "${HTMLPATHFROM}\cs_CZ\" "*"

   ${SetOutPath} "${HTMLPATHTO}\de_DE\Hints\"
   ${File} "${HTMLPATHFROM}\de_DE\Hints\" "*.html"

; empty
;   ${SetOutPath} "${HTMLPATHTO}\de_DE\"
;   ${File} "${HTMLPATHFROM}\de_DE\" "*"

   ${SetOutPath} "${HTMLPATHTO}\en_US\Hints\"
   ${File} "${HTMLPATHFROM}\en_US\Hints\" "*.html"

   ${SetOutPath} "${HTMLPATHTO}\en_US\"
   ${File} "${HTMLPATHFROM}\en_US\" "*"

   ${SetOutPath} "${HTMLPATHTO}\es_ES\Hints\"
   ${File} "${HTMLPATHFROM}\es_ES\Hints\" "*.html"

; empty
;   ${SetOutPath} "${HTMLPATHTO}\es_ES\"
;   ${File} "${HTMLPATHFROM}\es_ES\" "*"


   ${SetOutPath} "${HTMLPATHTO}\fi_FI\Hints\"
   ${File} "${HTMLPATHFROM}\fi_FI\Hints\" "*.html"

; empty
;   ${SetOutPath} "${HTMLPATHTO}\fi_FI\"
;   ${File} "${HTMLPATHFROM}\fi_FI\" "*"

   ${SetOutPath} "${HTMLPATHTO}\fr_FR\Hints\"
   ${File} "${HTMLPATHFROM}\fr_FR\Hints\" "*.html"

   ${SetOutPath} "${HTMLPATHTO}\fr_FR\"
   ${File} "${HTMLPATHFROM}\fr_FR\" "*"

   ${SetOutPath} "${HTMLPATHTO}\sl_SI\Hints\"
   ${File} "${HTMLPATHFROM}\sl_SI\Hints\" "*.html"

; empty
;   ${SetOutPath} "${HTMLPATHTO}\sl_SI\"
;   ${File} "${HTMLPATHFROM}\sl_SI\" "*"

   ${SetOutPath} "${HTMLPATHTO}\zh_CN\Hints\"
   ${File} "${HTMLPATHFROM}\zh_CN\Hints\" "*.html"

; empty
;   ${SetOutPath} "${HTMLPATHTO}\zh_CN\"
;   ${File} "${HTMLPATHFROM}\zh_CN\" "*"

   ${SetOutPath} "${HTMLPATHTO}\zh_TW\Hints\"
   ${File} "${HTMLPATHFROM}\zh_TW\Hints\" "*.html"

; empty
;   ${SetOutPath} "${HTMLPATHTO}\zh_TW\"
;   ${File} "${HTMLPATHFROM}\zh_TW\" "*"

SectionEnd

Section "serverhtml" SEC05

   ${SetOutPath} "${MOPATHTO}"
   ${File} "${MOPATHFROM}\" "pgadmin3.lng"

   ${SetOutPath} "${MOPATHTO}"
   ${File} "${MOPATHFROM}\" "pg_settings.csv"

   ${SetOutPath} "${MOPATHTO}\af_ZA\"
   ${File} "${MOPATHFROM}\af_ZA\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\ca_ES"
   ${File} "${MOPATHFROM}\ca_ES\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\de_DE"
   ${File} "${MOPATHFROM}\de_DE\" "*.mo"

;disapire
;   ${SetOutPath} "${MOPATHTO}\fi_FI"
;   ${File} "${MOPATHFROM}\fi_FI\" "*.mo"

;disapire
;   ${SetOutPath} "${MOPATHTO}\gl_ES"
;   ${File} "${MOPATHFROM}\gl_ES\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\ja_JP"
   ${File} "${MOPATHFROM}\ja_JP\" "*.mo"

;disapire
;   ${SetOutPath} "${MOPATHTO}\lv_LV"
;   ${File} "${MOPATHFROM}\lv_LV\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\pl_PL"
   ${File} "${MOPATHFROM}\pl_PL\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\pt_PT"
   ${File} "${MOPATHFROM}\pt_PT\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\sr_RS"
   ${File} "${MOPATHFROM}\sr_RS\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\zh_TW"
   ${File} "${MOPATHFROM}\zh_TW\" "*.mo"

;   ${SetOutPath} "${MOPATHTO}\bg_BG"
;   ${File} "${MOPATHFROM}\bg_BG\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\cs_CZ"
   ${File} "${MOPATHFROM}\cs_CZ\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\es_ES"
   ${File} "${MOPATHFROM}\es_ES\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\fr_FR"
   ${File} "${MOPATHFROM}\fr_FR\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\it_IT"
   ${File} "${MOPATHFROM}\it_IT\" "*.mo"

;  ${SetOutPath} "${MOPATHTO}\ko_KR"
;   ${File} "${MOPATHFROM}\ko_KR\" "*.mo"

;   ${SetOutPath} "${MOPATHTO}\nl_NL"
;   ${File} "${MOPATHFROM}\nl_NL\" "*.mo"

;   ${SetOutPath} "${MOPATHTO}\pt_BR"
;   ${File} "${MOPATHFROM}\pt_BR\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\ru_RU"
   ${File} "${MOPATHFROM}\ru_RU\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\zh_CN"
   ${File} "${MOPATHFROM}\zh_CN\" "*.mo"

   ${SetOutPath} "${MOPATHTO}\de_DE"
   ${File} "${MOPATHFROM}\de_DE\" "*.mo"

SectionEnd



;Section "serverhtml" SEC05
;WARNING ��� ��� ����, �� �� ����� ���� �� ��� ����. ������ �� ���������������
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${INTERFACESFROM}\ecpg\" "libecpg.dll"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "libecpg_compat.dll"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "libpgtypes.dll"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "ecpg.exe"

;SectionEnd




;Section "serverpljava" SEC06
;WARNING �� �� ���������� pljava
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${PLPATHFROM}\pg_ctl\" "pljava.dll"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${PLPATHFROM}\pljava\src\" "pljava.jar"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${PLPATHFROM}\pljava\src\" "examples.jar"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${PLPATHFROM}\pljava\src\" "install.sql"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${PLPATHFROM}\pljava\src\" "uninstall.sql"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${PLPATHFROM}\pljava\src\" "deploy.jar"
;SectionEnd

Section "serverpsql" SEC07

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${BINPAHFROM}\psql\" "psql.exe"

;WARNING �� ����, ��� ����� �����
;   ${SetOutPath} "${BINPATHTO}\"
;   ${File} "${BINPAHFROM}\psql\" "psql.bat"

SectionEnd

;Section "serverhelppgadmin" SEC08
;WARNING Help pgadmin �����, ��� �� ����� ������
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "pgadmin3.chm"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "pgAdmin Help"
;SectionEnd


;Section "serverdoc" SEC09
;WARNING ������ ����. ����� postgresql.chm, ������� �� ����� �� ��������������.
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "postgresql.chm"
   
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "COPYRIGHT.TXT"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "intro.html"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "jni_rationale.html"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "readme.html"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "solutions.html"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "userguide.html"

;SectionEnd


;Section "serverdoc" SEC09
;WARNING pldebugger �� ��������
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${CONTRIBPATHFROM}\plugin_debugger\" "plugin_debugger.dll"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${CONTRIBPATHFROM}\pg_ctl\" "plugin_profiler.dll"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "pldbgapi.dll"

;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "targetinfo.dll"
;SectionEnd

Section "serverelse" SEC10
   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\pgbench\" "pgbench.exe" 

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${CONTRIBPATHFROM}\vacuumlo\" "vacuumlo.exe"


;�� ������� ��� ���. �� ����� �� ���������������.
;   ${SetOutPath} "${LIBPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "EventMessagefile"

; �� ����, ��� �����, ���� ��������.
;   ${SetOutPath} ""
;   ${File} "Installation Notes.rtf"


   ${SetOutPath} "$INSTDIR\"
   ${File}  "${SSLPATH}\" "License.txt"

   ${Rename} "$INSTDIR\License.txt" "$INSTDIR\OpenSSL License.txt"

   ${SetOutPath} "${BINPATHTO}"
   ${File} "${INTERFACESFROM}\libpq\" "libpq-90eter.dll"

   ${CopyFiles} "${BINPATHTO}\libpq-90eter.dll" "${BINPATHTO}\libpq.dll"


   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SSLPATH}\" "ssleay32.dll"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${SSLPATH}\" "libeay32.dll"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${PGADMINPATHFROM}\" "iconv.dll"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${PGADMINPATHFROM}\" "libintl-8.dll"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${PGADMINPATHFROM}\" "libiconv-2.dll"

;   ${SetOutPath} "${BINPATHTO}\"
;   ${File} "${PGADMINPATHFROM}\" "comerr32.dll"

;   ${SetOutPath} "${BINPATHTO}\"
;   ${File} "${PGADMINPATHFROM}\" "gssapi32.dll"

;   ${SetOutPath} "${BINPATHTO}\"
;   ${File} "${PGADMINPATHFROM}\" "k5sprt32.dll"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${PGADMINPATHFROM}\" "libxslt.dll"

;WARNING krb �� �� ����������
;   ${SetOutPath} ""
;   ${File} "krb5_32.dll"

;   ${SetOutPath} ""
;   ${File} "k5sprt32.dll"

; ���� ��� krb5
;   ${SetOutPath} "${BINPATHTO}"
;   ${File} "gssapi32.dll"

; ���� ��� krb5
;   ${SetOutPath} ""
;   ${File} "comerr32.dll"


   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${LIBMINGWPATHFROM}\" "libxml2-2.dll"

   ${CopyFiles} "${BINPATHTO}\libxml2-2.dll" "${BINPATHTO}\libxml2.dll"


;WARNING �� �������� libxslt.dll ������ ���� mingw32-libltdl
;   ${SetOutPath} ""
;   ${File} "libxslt.dll"

   ${SetOutPath} "${BINPATHTO}"
   ${File} "${LIBMINGWPATHFROM}\" "zlib1.dll"

;   ${SetOutPath} "${BINPATHTO}"
;   ${File} "${MSVCRPATHFROM}\" "msvcr71.dll"


;WARNING StackBuilder �� ��������. ����� �� �����
;   ${SetOutPath} "${BINPATHTO}\"
;   ${File} "${BINPAHFROM}\pg_ctl\" "StackBuilder.exe"

   ${SetOutPath} "${BINPATHTO}\"
   ${File} "${PGADMINPATHFROM}\" "pgAdmin3.exe"

;WARNING ��������� � �� ��� ����������
;   ${SetOutPath} "${BINPATHTO}\"
;   ${File} "${CONTRIBPATHFROM}\oid2name\" "oid2name.exe"



SectionEnd



Section "serverelse" SEC11 

${SetOutPath} "${SHAREPATHTO}\"

${File} "${BACKENDPATHFROM}\utils\mb\conversion_procs\" "conversion_create.sql"
${File} "${BACKENDPATHFROM}\catalog\" "information_schema.sql"
${File} "${BACKENDPATHFROM}\libpq\" "pg_hba.conf.sample"
${File} "${BACKENDPATHFROM}\libpq\" "pg_ident.conf.sample"
${File} "${INTERFACESFROM}\libpq\" "pg_service.conf.sample"
${File} "${BACKENDPATHFROM}\catalog\" "postgres.bki"
${File} "${BACKENDPATHFROM}\catalog\" "postgres.description"
${File} "${BACKENDPATHFROM}\catalog\" "postgres.shdescription"
${File} "${BACKENDPATHFROM}\utils\misc\" "postgresql.conf.sample"
${File} "${BINPAHFROM}\psql\" "psqlrc.sample"
${File} "${BACKENDPATHFROM}\access\transam\" "recovery.conf.sample"
${File} "${BACKENDPATHFROM}\snowball\" "snowball_create.sql"
${File} "${BACKENDPATHFROM}\catalog\" "sql_features.txt"
${File} "${BACKENDPATHFROM}\catalog\" "system_views.sql"
${File} "${PLPATHFROM}\tcl\modules\" "unknown.pltcl"

${SetOutPath} "${SHAREPATHTO}\timezone\"
${File} "${TIMEZONEFROM}\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\"
${File} "${TIMEZONEFROM}\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Africa\"
${File} "${TIMEZONEFROM}\Africa\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\America\"
${File} "${TIMEZONEFROM}\America\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Antarctica\"
${File} "${TIMEZONEFROM}\Antarctica\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Arctic\"
${File} "${TIMEZONEFROM}\Arctic\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Asia\"
${File} "${TIMEZONEFROM}\Asia\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Atlantic\"
${File} "${TIMEZONEFROM}\Atlantic\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Australia\"
${File} "${TIMEZONEFROM}\Australia\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Brazil\"
${File} "${TIMEZONEFROM}\Brazil\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Canada\"
${File} "${TIMEZONEFROM}\Canada\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Chile\"
${File} "${TIMEZONEFROM}\Chile\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Etc\"
${File} "${TIMEZONEFROM}\Etc\" "*"
 
${SetOutPath} "${SHAREPATHTO}\timezone\Europe\"
${File} "${TIMEZONEFROM}\Europe\" "*" 
 
${SetOutPath} "${SHAREPATHTO}\timezone\Indian\"
${File} "${TIMEZONEFROM}\Indian\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Mexico\"
${File} "${TIMEZONEFROM}\Mexico\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Mideast\"
${File} "${TIMEZONEFROM}\Mideast\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\Pacific\"
${File} "${TIMEZONEFROM}\Pacific\" "*"

${SetOutPath} "${SHAREPATHTO}\timezone\US\"
${File} "${TIMEZONEFROM}\US\" "*"

${SetOutPath} "${SHAREPATHTO}\timezonesets\"
${File} "${TZNAMESFROM}\" "*"

${SetOutPath} "${SHAREPATHTO}\tsearch_data\"
${File} "${BACKENDPATHFROM}\snowball\stopwords\" "*.stop"

SectionEnd

