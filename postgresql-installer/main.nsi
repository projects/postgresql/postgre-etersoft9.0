!include "AdditionalMacro.nsi" 		; ���� � ��������� ��� ����� ������������� ������
!include "CheckAdmin.nsi" 		; ���� ������� �������� �� ��������������. 
!include "setting.nsi" 			; ���� ������� (���� � �.�.)
!include "copyfiles.nsi" 		; ���� � ������ ������
!include "dialogsCustom.nsi" 		; ���� � ��������� ��� �������
!include "dialogService.nsi" 		; ���� � �������� ��� ��������
!include "dialogMakeClaster.nsi" 	; ���� � �������� ��� �������� ��������
!include "dialogFinish.nsi" 		; ���� � ��������� ��������
!include "writereg.nsi" 		; ���� � ������� � ������
!include "RunService.nsi" 		; ���� c �������� ��� ������� �������
!include "createstartshortcut.nsi"	; ���� ������ ������ � ������ ����
!include "SetUninstaller.nsi" 		; ���� � ����������� �������������
!include "installvcredist.nsi" 		; 

InstallDir "$PROGRAMFILES\PostgreSQL\${VERPG}-${sufix}${REVPG}"

LicenseText "���������� ���������� ������������ ���������� ����� ����������� ${NAMEPG} ${VERPG}-${sufix}${REVPG}. ���� �� ���������� ��� ������� ����������, ������� ��������." "��������"
BrandingText "Etersoft �  2011"

DirText "��������� ����� ����������� � ������� ����������. ��� ������� � ������ ���������� ������� ������ ����� � ������� ������ ���������." "���� ���������" "�����"

MiscButtonText "< �����" "����� >" "������" "�������"
InstallButtonText "���������"
DetailsButtonText "������..."

Name "${NAMEPG}-${VERMAJOR}eter-${REVPG}"
OutFile "${NAMEFILE}${VERMAJOR}-${VERMAJOR}.${VERMINOR}-${REVPG}.exe"
LicenseData "${LICENSEPATH}/license.txt"

Icon "${ICOPATH}/postgres.ico"
UninstallIcon "${ICOPATH}/postgresun.ico"



Page custom CheckAdmin
Page custom installvcredist
Page license
Page directory
Page custom CustomMakeClusterCreate CustomMakeClusterLeave
Page custom CustomServiceCreate CustomServiceLeave
Page instfiles
Page custom CreateUserPostgres
Page custom CustomMakeClusterMake
Page custom RunService


Page custom SaveReg
Page custom createstartshortcut
Page custom CustomSetUninstaller 
Page custom CustomFinishCreate

;UninstPage uninstconfirm
UninstPage instfiles
