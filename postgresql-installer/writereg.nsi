!include "FileFunc.nsh"
!define MAINKEY 	"HKLM"





!define SERVICEID	"${SMALNAMEPG}-${VERPG}-${sufix}${REVPG}"

!define REGPG 		"Software\PostgreSQL\"
!define REGINTPG 	"${REGPG}Installations\{${PRODUCTCODE}}"
!define REGSERVICE 	"${REGPG}Services\${SERVICEID}"
!define REGAPPLIC	"SYSTEM\CurrentControlSet\Services\EventLog\Application\PostgreSQL"
!define REGPGADMIN 	"Software\pgAdmin III"


!define REGUNINSTALLER 	"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{${PRODUCTCODE}}"

!define VERSION 	${VERPG}
;!define SERVICENAME	 ; ������� �� dialogs
;!define SERVICEACCOUNT	${NAMEPG} ; ������� �� dialogs
!define PGEVENT		""




Function SaveReg

var /GLOBAL BASEDIR 
var /GLOBAL DATADIR
var /GLOBAL BINDIR

strcpy $BASEDIR "$INSTDIR"
strcpy $DATADIR "$INSTDIR\Data"
strcpy $BINDIR "$INSTDIR\Bin"

WriteRegStr	${MAINKEY}	${REGINTPG}		"Base Directory"	"$BASEDIR"
WriteRegStr	${MAINKEY}	${REGINTPG}		"Data Directory"	"$DATADIR"
WriteRegStr	${MAINKEY}	${REGINTPG}		"Version" 	 	${VERSION}
WriteRegStr	${MAINKEY}	${REGSERVICE}		"Display Name" 		"$SERVICENAME"
WriteRegStr	${MAINKEY}	${REGSERVICE}		"Service Account"	"$SERVICEDOMAIN\$SERVICEACCOUNT"
WriteRegStr	${MAINKEY}	${REGSERVICE}		"Data Directory"	"$DATADIR"
WriteRegDWORD	${MAINKEY}	${REGSERVICE}		"Port"			"$PORT"
WriteRegStr	${MAINKEY}	${REGSERVICE}		"Database Superuser"	"$SUNAME"
WriteRegStr	${MAINKEY}	${REGSERVICE}		"Encoding"		"$SERVERENCODING"
WriteRegStr	${MAINKEY}	${REGSERVICE}		"Locale"		"$LOCALE"
WriteRegStr	${MAINKEY}	${REGSERVICE}		"Product Code"		"{${PRODUCTCODE}}"
WriteRegStr	${MAINKEY}	${REGINTPG}		"Service ID"		"${SERVICEID}"
WriteRegStr	${MAINKEY}	"${REGPGADMIN}"		"Helper Path"		"$BINDIR"
WriteRegStr	${MAINKEY}	"${REGAPPLIC}" 		"EventMessageFile"	"${PGEVENT}"


WriteRegStr	"HKLM"		"${REGUNINSTALLER}" 	"DisplayName"		"${NAMEPG} ${VERPG}eter-${REVPG}"
WriteRegStr	"HKLM"		"${REGUNINSTALLER}" 	"UninstallString"	"$BASEDIR\uninst.exe"
${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
 IntFmt $0 "0x%08X" $0
WriteRegDWORD 	"HKLM" 		"${REGUNINSTALLER}" 	"EstimatedSize" "$0"




FunctionEnd
