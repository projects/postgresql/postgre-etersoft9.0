Function CheckUserPostgres
	UserMgr::GetUserInfoEx "$SERVICEACCOUNT" "$SERVICEDOMAIN" "EXISTS"
	Pop $0
	
	
	StrCmp $0 "OK" 0 OKCHECK
	MessageBox MB_OK|MB_ICONINFORMATION \
	"������������ � ����� ������ ��� ����������" IDOK

	goto ERRORCHECK

	OKCHECK:
	strcpy $0 "OK"
	goto ENDCHECKUSERPOSTGRES

	ERRORCHECK:
	strcpy $0 "ERROR"
	goto ENDCHECKUSERPOSTGRES

	ENDCHECKUSERPOSTGRES:
FunctionEnd


Function CreateUserPostgres

	UserMgr::CreateAccountEx2 "$SERVICEACCOUNT" "$SERVICEDOMAIN" $SERVICEPASSWORD "postgres" "PostgreSQL service account" "UF_DONT_EXPIRE_PASSWD" 
;UF_PASSWD_NOTREQD|
	Pop $0

CHECKCREATEACCOUNT:	
	StrCmp $0 "OK" 0 ERRORCREATEACCOUNT
	goto ADDPRIVELEGE
ERRORCREATEACCOUNT:
;	StrCmp $0 "ERROR 2221" 0 ERRORCREATEACCOUNT
;	goto ENDCREATEUSERPOSTGRES
	MessageBox MB_OK|MB_ICONINFORMATION \
	"������ �������� ������������ $0" IDOK
ADDPRIVELEGE:
	UserMgr::AddPrivilege "$SERVICEACCOUNT" "SeServiceLogonRight"
	Pop $0
	StrCmp $0 "OK" 0 ERRORADDPRIVELEGE
	goto ADDPATHACCESS
ERRORADDPRIVELEGE:
	MessageBox MB_OK|MB_ICONINFORMATION \
	"������ ���������� ���������� $\"Log on as a service$\" $0" IDOK
ADDPATHACCESS:
	UserMgr::SetPathAccess "$SERVICEACCOUNT" "$INSTDIR" "=a"
	Pop $0
	StrCmp $0 "OK" 0 ERRORADDPATHACCESS
	goto ENDCREATEUSERPOSTGRES
ERRORADDPATHACCESS:
	MessageBox MB_OK|MB_ICONINFORMATION \
	"������ ��������� ���� ������� $0" IDOK
ENDCREATEUSERPOSTGRES:



FunctionEnd


Function RunService
	var /GLOBAL SERVICE_COMMANDLINE
	strcpy $BINDIR "$INSTDIR\Bin"
	strcpy $DATADIR "$INSTDIR\Data"
	nsSCM::QueryStatus ${SERVICEDISPLAYNAME}
	Pop $0 ; return error/success
  	Pop $1 ; return service status
	StrCmp $0 "success" 0 INSTALLSERVICE
	goto ERRORQUERYSTATUS
ERRORQUERYSTATUS:
	MessageBox MB_OK|MB_ICONINFORMATION \
	"������ � ����� ������ ��� ����������" IDOK
	goto STARTSERVICE

INSTALLSERVICE:
	strcpy $SERVICE_COMMANDLINE "$\"$BINDIR\pg_ctl.exe$\" runservice -N $\"${SERVICEDISPLAYNAME}$\" -D $\"$DATADIR$\" -P $PORT"

	nsSCM::Install /NOUNLOAD "${SERVICEDISPLAYNAME}" "$SERVICENAME" 16 2  "$SERVICE_COMMANDLINE" "" "" ".\$SERVICEACCOUNT" "$SERVICEPASSWORD"
	Pop $0 
	StrCmp $0 "success" 0 ERRORINSTALLSERVICE
	goto STARTSERVICE
ERRORINSTALLSERVICE:
	MessageBox MB_OK|MB_ICONINFORMATION \
	"������ �������� ������� $0" IDOK
STARTSERVICE:
	nsSCM::Start ${SERVICEDISPLAYNAME}
	Pop $0 ; return error/success
	StrCmp $0 "success" 0 ERRORSTARTSERVICE
	goto ENDALLSERVICE
ERRORSTARTSERVICE:
	MessageBox MB_OK|MB_ICONINFORMATION \
	"������ ������� ������� $0" IDOK
ENDALLSERVICE:


FunctionEnd
