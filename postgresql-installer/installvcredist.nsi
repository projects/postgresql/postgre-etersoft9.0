

Function installvcredist

strcpy $1 0
DetailPrint "1 $1"
; SectionIn RO

; Create a folder to temporarily hold Microsoft DLLs to ultimately be installed into the SxS folder.
InitPluginsDir

DetailPrint "pluginsdir $PLUGINSDIR"


ClearErrors
EnumRegValue $1 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{1F1C2DFC-2D24-3E06-BCB8-725134ADF989}" $0
IfErrors start
goto done
start:

SetOutPath $PLUGINSDIR
SetOverwrite on ;устанавливает режим перезаписи без запроса

SetOutPath "$PLUGINSDIR"
File "${MSVCRPATHFROM}\vcredist_x86.exe"

ExecWait "$PLUGINSDIR\vcredist_x86.exe" ;запускаем установку и ждём её завершения
done:


FunctionEnd
