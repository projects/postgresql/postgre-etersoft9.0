#!/bin/bash

RPMDIR=$1
PGRELEASE=$2
PGVERSION=$3
PGNAME=postgresql
WORKDIR=..
PGINSTALLERDIR=$RPMDIR/BUILD/$PGNAME*/$PGNAME-installer-$PGVERSION
PGWINDIR=$RPMDIR/BUILD/$PGNAME*/$PGNAME-win-$PGVERSION

PGSSLPATH=/var/ftp/pvt/Windows/4makepg/nsis/OpenSSL-Win32
PGLIBMINGWPATH=/usr/i586-pc-mingw32/sys-root/mingw/bin
PGMSVCRPATH="\\var\\ftp\\pvt\\Windows\\4makepg\\nsis\\msvcr\\90\\"
PGADMINPATH="\\var\\ftp\\pvt\\Windows\\4makepg\\nsis\\pgAdmin III\\1.12\\"
PGTIMEZONE=/var/ftp/pvt/Windows/4makepg/nsis/timezone
WINCONFIGOPTIONS="--host i586-pc-mingw32 --without-zlib --without-readline --enable-nls --without-openssl --without-perl --without-python --without-tcl ZIC=/usr/sbin/zic"
ICUURL=http://download.icu-project.org/files/icu4c/4.2.1/icu4c-4_2_1-Win32-msvc9.zip
ICUFILE=icu4c-4_2_1-Win32-msvc9.zip
PGVERMAJOR=9.0
PGVERMINOR=2

switch_to_win_path()
{
    LINPATH="$1"
    echo "$LINPATH" | sed "s|/|\\\\\\\\|g"
}

configure_pg()
{
	sed -i "s|NAME= pq-9.0eter|NAME= pq-90eter|" src/interfaces/libpq/Makefile
	sed -i "s|-L\$(libpq_builddir) -lpq-9.0eter|-L\$(libpq_builddir) -lpq-90eter|" src/Makefile.global.in
	find . -type f -name Makefile | xargs sed -i 's|libpq-9.0eter\.a|libpq-90eter.a|g'
	./configure $WINCONFIGOPTIONS \
	    --with-libxml \
            --disable-integer-datetimes \
	    --disable-rpath
}

prepare_pg_for_win()
{
    pushd contrib/mchar/
    #TODO: remove this after eterbug 5958 is solved
	wget $ICUURL
	unzip $ICUFILE
	sed -i 's|PG_CPPFLAGS=-I/usr/local/include|PG_CPPFLAGS=-I/usr/local/include -Iicu/include|' Makefile
	sed -i "s|SHLIB_LINK += -L/usr/local/lib -licuuc -l\$(ICUNAME) -Wl,-rpath,'\$\$ORIGIN'|SHLIB_LINK += -Licu/lib -licuuc -l\$(ICUNAME) -Wl,-rpath,'\$\$ORIGIN'|" Makefile
    #
    popd
    pushd contrib/uuid-ossp/
	make
	ls -l | grep sql || exit 1
    popd
    for i in contrib/pg_standby contrib/pgbench contrib/vacuumlo contrib/oid2name ; do
	PROG_NAME=`basename $i`
	ls -l $i/Makefile
	sed -i "s|PROGRAM = $PROG_NAME|PROGRAM = $PROG_NAME.exe|" $i/Makefile
    done
}


set_win_vars()
{
    echo "Setting installer vars"
    WINWORKDIR="$(switch_to_win_path $WORKDIR)"
    WINSSLPATH="$(switch_to_win_path $PGSSLPATH)"
    WINLIBMINGWPATH="$(switch_to_win_path $PGLIBMINGWPATH)"
    WINMSVCRPATH="$(switch_to_win_path $PGMSVCRPATH)"
    WINPGADMINPATH="$(switch_to_win_path "$PGADMINPATH")"
    WINTIMEZONE="$(switch_to_win_path $PGTIMEZONE)"
}

make_installer()
{
	sed -i "s|define VERPG.*$|define VERPG \"$PGVERSION\"|" setting.nsi
	sed -i "s|define VERMAJOR.*$|define VERMAJOR \"$PGVERMAJOR\"|" setting.nsi
	sed -i "s|define VERMINOR.*$|define VERMINOR \"$PGVERMINOR\"|" setting.nsi
	sed -i "s|define REVPG.*$|define REVPG \"$PGRELEASE\"|" setting.nsi
	sed -i "s|define PGHEADPATHFROM.*$|define PGHEADPATHFROM \"$WINWORKDIR\"|" setting.nsi
	sed -i "s|define SSLPATH.*$|define SSLPATH \"$WINSSLPATH\"|" setting.nsi
	sed -i "s|define LIBMINGWPATHFROM.*$|define LIBMINGWPATHFROM \"$WINLIBMINGWPATH\"|" setting.nsi
	sed -i "s|define MSVCRPATHFROM.*$|define MSVCRPATHFROM \"$WINMSVCRPATH\"|" setting.nsi
	sed -i "s|define PGADMINPATHFROM.*$|define PGADMINPATHFROM \"$WINPGADMINPATH\"|" setting.nsi
	sed -i "s|define TIMEZONEFROM.*$|define TIMEZONEFROM \"$WINTIMEZONE\"|" setting.nsi
	make
}

pushd ..
	configure_pg
popd

pushd .. || exit 1
    prepare_pg_for_win

    make -j4
    make -C contrib all
#    maybe unnecessary with --with-xml configure option
#    make -C contrib/xml2 all

    echo "Testing for built binaries"
    for i in contrib/pg_standby contrib/pgbench contrib/vacuumlo contrib/oid2name ; do
	echo "Switching to $i"
	ls -l $i | grep exe || fatal "No binary files were built in $i"
    done
popd

set_win_vars
pushd $PGINSTALLERDIR
    make_installer
popd

exit 0