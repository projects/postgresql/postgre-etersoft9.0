#redefine __patch for strict patching
#define __patch /usr/bin/patch --fuzz=0


%define _disable_ld_no_undefined 1
%define beta 0
%define plpython 1
%define pltcl 0
%define plperl 1
#define tcl 0
%define nls 1
%define pgfts 1
%define runselftest 0
%define xml 1

%define ssl 0
%define pam 0
%define test 0
%define kerberos 0

%if %_vendor == "alt"
%define icu 0
%else
# build with embedded libicu if 1
# FIXME: need to drop build with embedded
%define icu 1
%endif

%{!?aconfver:%define aconfver autoconf}
%define kerbdir "/usr"
%define pgbaseinstdir /usr/pgsql

%{!?test:%define test 1}
%{!?icu:%define icu 1}
%{!?plpython:%define plpython 1}
%{!?pltcl:%define pltcl 1}
%{!?plperl:%define plperl 1}
%{!?ssl:%define ssl 1}
%{!?kerberos:%define kerberos 1}
%{!?nls:%define nls 1}
%{!?xml:%define xml 1}
%{!?pam:%define pam 0}
%{!?pgfts:%define pgfts 1}
%{!?runselftest:%define runselftest 0}

Name: postgre-etersoft9.0
Version: 9.0.2
Release: alt8

Summary: PostgreSQL client programs and libraries (Etersoft edition for Selta and 1C)

License: BSD
Group: Databases
Url: http://www.postgresql.org/

Packager: Alexander Shevelev <alexalv@etersoft.ru>

Source: ftp://ftp.postgresql.org/pub/source/v%version/postgresql-%version.tar
Source2: postgresql.outformat
Source3: postgresql.init
Source4: Makefile.regress
Source5: pg_config.h
Source6: README.rpm-dist
Source12: http://www.postgresql.org/files/documentation/pdf/9.0/postgresql-9.0-A4.pdf
Source14: postgresql.pam
Source15: postgresql-bashprofile
Source16: filter-requires-perl-Pg.sh
Source17: postgresql.sysconfig
#sources for windows
Source18: postgresql-win-%version.tar
Source19: postgresql-installer-%version.tar

#Patch1: rpm-pgsql.patch
Patch3: postgresql-logging.patch
#Patch6: postgresql-perl-rpath.patch
Patch8: postgresql-prefer-ncurses.patch
Patch9: 1c_FULL_90-0.19.patch
Patch10: postgresql-1c-9.0.patch
Patch11: applock-1c-8.4.1.patch
Patch12: timestamp.c.diff
Patch13: postgresql.conf.sample.patch
Patch14: postgres_datalen.patch
# Etersoft's fix for 1c patches
Patch15: 1cfix_etersoft.patch
Patch16: selta_etersoft.patch
Patch50: eter-rename-libpq.patch

Patch51: postgresql-tmp_table_cleanup.patch
Patch52: postgresql-no_transaction_blocks.patch

BuildRequires: rpm-build-compat
BuildRequires: perl glibc-devel bison flex autoconf perl-devel
BuildRequires: libreadline-devel
BuildRequires: zlib-devel >= 1.0.4

%if %icu
# require our build of ICU
BuildRequires: libicu38-devel = 3.8
BuildRequires: libicu38 = 3.8
%else
BuildRequires: libicu-devel
%endif

%if %plpython
BuildPreReq: python-devel
%endif

%if %pltcl
BuildRequires: tcl-devel
%endif

%if %ssl
BuildRequires: openssl-devel
%endif

%if %kerberos
BuildRequires: krb5-devel
BuildRequires: e2fsprogs-devel
%endif

%if %nls
BuildRequires: gettext >= 0.10.35
%endif

%if %xml
BuildRequires: libxml2-devel libxslt-devel
%endif

%if %pam
BuildRequires: pam-devel
%endif

Requires: libpq5.2-9.0eter = %version-%release

%if %_vendor == "alt"
Requires: postgresql-common
%else
Provides: postgresql
Provides: postgre-etersoft9.0
%endif

%description
PostgreSQL is an advanced Object-Relational database management system
(DBMS) that supports almost all SQL constructs (including
transactions, subselects and user-defined types and functions). The
postgresql package includes the client programs and libraries that
you'll need to access a PostgreSQL DBMS server.  These PostgreSQL
client programs are programs that directly manipulate the internal
structure of PostgreSQL databases on a PostgreSQL server. These client
programs can be located on the same machine with the PostgreSQL
server, or may be on a remote machine which accesses a PostgreSQL
server over a network connection. This package contains the command-line
utilities for managing PostgreSQL databases on a PostgreSQL server.

If you want to manipulate a PostgreSQL database on a local or remote PostgreSQL
server, you need this package. You also need to install this package
if you're installing the postgresql-server package.

Etersoft edition appointed for SELTA@Etersoft and 1C Enterprise Server

%package -n libpq5.2-9.0eter
Summary: The shared libraries required for any PostgreSQL clients
Group: Databases
Obsoletes: libpq5.2

%description -n libpq5.2-9.0eter
The postgresql-libs package provides the essential shared libraries for any
PostgreSQL client program or interface. You will need to install this package
to use any other PostgreSQL package or any clients that need to connect to a
PostgreSQL server.

%package server
Summary: The programs needed to create and run a PostgreSQL server
Group: Databases
Requires: %name = %version-%release
Conflicts: postgresql8.4 postgresql8.3 postgresql8.2 postgresql8.1 postgresql8.0 postgresql-8.2eter postgresql-8.3eter postgre-etersoft8.4

%description server
The postgresql-server package includes the programs needed to create
and run a PostgreSQL server, which will in turn allow you to create
and maintain PostgreSQL databases.  PostgreSQL is an advanced
Object-Relational database management system (DBMS) that supports
almost all SQL constructs (including transactions, subselects and
user-defined types and functions). You should install
postgresql-server if you want to create and maintain your own
PostgreSQL databases and/or your own PostgreSQL server. You also need
to install the postgresql package.

%package docs
Summary: Extra documentation for PostgreSQL
Group: Databases
%description docs
The postgresql-docs package includes the SGML source for the documentation
as well as the documentation in PDF format and some extra documentation.
Install this package if you want to help with the PostgreSQL documentation
project, or if you want to generate printed documentation. This package also
includes HTML version of the documentation.

%package contrib
Summary: Contributed source and binaries distributed with PostgreSQL
Group: Databases
Requires: %name = %version-%release
Conflicts: postgresql8.4-contrib postgresql8.3-contrib postgresql8.2-contrib postgresql8.1-contrib postgresql8.0-contrib
Conflicts: postgresql-8.2eter-contrib postgresql-8.3eter-contrib
%add_findprov_lib_path %_libdir/pgsql

%description contrib
The postgresql-contrib package contains contributed packages that are
included in the PostgreSQL distribution.
This package include mchar additional module.

%package seltaaddon
Summary: Selta addon for postgresql
Group: Databases
Requires: %name = %version-%release
%add_findprov_lib_path %_libdir/pgsql

%description seltaaddon
Selta addon for postgresql

%if %plperl
%package plperl
Summary: The Perl procedural language for PostgreSQL
Group: Databases
Requires: %name = %version-%release
Requires: %name-server = %version-%release
Obsoletes: postgresql-pl

%description plperl
PostgreSQL is an advanced Object-Relational database management
system. The postgresql-plperl package contains the PL/Perl language
for the backend.
%endif

%if %plpython
%package plpython
Summary: The Python procedural language for PostgreSQL
Group: Databases
Requires: %name = %version-%release
Requires: %name-server = %version-%release
Obsoletes: postgresql-pl

%description plpython
PostgreSQL is an advanced Object-Relational database management
system. The postgresql-plpython package contains the PL/Python language
for the backend.
%endif

%if %pltcl
%package pltcl
Summary: The Tcl procedural language for PostgreSQL
Group: Databases
Requires: %name = %version-%release
Requires: %name-server = %version-%release
Obsoletes: postgresql-pl

%description pltcl
PostgreSQL is an advanced Object-Relational database management
system. The postgresql-pltcl package contains the PL/Tcl language
for the backend.
%endif

%if %test
%package test
Summary: The test suite distributed with PostgreSQL
Group: Databases
Requires: %name = %version-%release
Requires: %name-server = %version-%release

%description test
PostgreSQL is an advanced Object-Relational database management
system. The postgresql-test package includes the sources and pre-built
binaries of various tests for the PostgreSQL database management
system, including regression tests and benchmarks.
%endif

%define __perl_requires %SOURCE16

%prep
%setup -a 18 -a 19 -n postgresql-%version
#TODO: next three lines. What is it?
cd doc/src/sgml
mkdir html
cd html
tar zxf ../../../postgres.tar.gz 
tar zxf ../../../man.tar.gz 
cd ../../../../

subst "s,PACKAGE_VERSION='%version',PACKAGE_VERSION='%version-%release',g" configure
subst "s,PACKAGE_STRING='PostgreSQL %version',PACKAGE_STRING='PostgreSQL %version Etersoft Edition',g" configure
subst "s,PACKAGE_BUGREPORT='pgsql-bugs\@postgresql.org',PACKAGE_BUGREPORT='support\@etersoft.ru',g" configure

# Patch1: rpm-pgsql.patch
#%patch1 -p1
# Patch4: postgresql-test.patch
# now test is by C program.
%patch3 -p1
# Patch6: postgresql-perl-rpath.patch
#%patch6 -p1
# Patch7: 1c_FULL_83-0.19.patch
#%patch7 -p2
# Patch8: postgresql-1c-8.4.patch
%patch8 -p1
# Patch9: applock-1c-8.3.1.patch
%patch9 -p0
# Patch10: timestamp.c.diff
%patch10 -p1
# Patch11: postgresql.conf.sample.patch
%patch11 -p0
# Patch12: pg_hba.conf.sample.patch
# already included in postgresql-1c-8.3.patch
#%patch12 -p1
# Patch13: postgres_datalen.patch
#%patch13 -p2
# Etersoft's fix for 1c patches
# Patch15: 1cfix_etersoft.patch
%patch14 -p2
%patch15 -p2
%patch16 -p2
# Etersoft rename libpq
%patch50 -p2

#Patch51: postgresql-tmp_table_cleanup.patch
#%patch51 -p1
#Patch52: postgres-no_transaction_blocks.patch
#%patch52 -p2

find . -type f -name Makefile | xargs sed -i 's|libpq\.a|libpq-9.0eter.a|g'

#cd doc
#tar -zcf postgres.tar.gz *.html stylesheet.css
#rm -f *.html stylesheet.css
#cd -

#cp -p %SOURCE12 .

%build
export LIBNAME=%_lib
%configure --disable-rpath \
%if %beta
	--enable-debug \
	--enable-cassert \
%endif
%if %plperl
	--with-perl \
%endif
%if %plpython
	--with-python \
%endif
%if %pltcl
	--with-tcl \
	--with-tclconfig=%_libdir \
%endif
%if %ssl
	--with-openssl \
%endif
%if %pam
	--with-pam \
%endif
%if %kerberos
	--with-krb5 \
	--with-includes=%kerbdir/include \
	--with-libraries=%kerbdir/%_lib \
%endif
%if %nls
	--enable-nls \
%endif
%if %pgfts
	--enable-thread-safety \
%endif
%if %xml
	--with-libxml \
	--with-libxslt \
%endif
	--disable-integer-datetimes \
	--sysconfdir=%_sysconfdir/sysconfig/pgsql \
	--datadir=%_datadir/pgsql \
	--docdir=%{_docdir}

%make_build all
%if %icu
export ORIGIN=%_libdir/pgsql
%endif
%make_build -C contrib all
%if %xml
%make_build -C contrib/xml2 all
%endif

# Have to hack makefile to put correct path into tutorial scripts
#sed "s|C=\`pwd\`;|C=%_libdir/pgsql/tutorial;|" < src/tutorial/Makefile > src/tutorial/GNUmakefile
#%make_build -C src/tutorial NO_PGXS=1 all
#rm -f src/tutorial/GNUmakefile
%make_build

%if %runselftest
cd src/test/regress
make all
make MAX_CONNECTIONS=5 check
make clean
cd -
%endif

%if %test
cd src/test/regress
make RPMTESTING=1 all
cd -
%endif

%install
# do not override INSTALL
%make install DESTDIR=%buildroot
%make install DESTDIR=%buildroot -C contrib
%if %xml
	%make install DESTDIR=%buildroot -C contrib/xml2
%endif

install -d %buildroot%_initdir/
sed 's/^PGVERSION=.*$/PGVERSION=%version/' <%SOURCE3 > postgresql.init
install -m 755 postgresql.init %buildroot%_initdir/postgresql
%if %_vendor == "alt"
%else
install -m 755 %SOURCE2 %buildroot%_initdir/postgresql.outformat
%endif

%if %pam
	install -d %buildroot%_sysconfdir/pam.d
	install -m 644 %SOURCE14 %buildroot%_sysconfdir/pam.d/postgresql
%endif

# PGDATA needs removal of group and world permissions due to pg_pwd hole.
install -d -m 700 %buildroot/var/lib/pgsql/data

# backups of data go here...
install -d -m 700 %buildroot/var/lib/pgsql/backups

# postgres' .bash_profile
install -m 644 %SOURCE15 %buildroot/var/lib/pgsql/.bash_profile

# Create the multiple postmaster startup directory
install -d -m 700 %buildroot%_sysconfdir/sysconfig/pgsql

%if %test
	# tests. There are many files included here that are unnecessary, but include
	# them anyway for completeness.
	mkdir -p %buildroot%_libdir/pgsql/test
	cp -a src/test/regress %buildroot%_libdir/pgsql/test
	install -m 0755 contrib/spi/refint.so %buildroot%_libdir/pgsql/test/regress
	install -m 0755 contrib/spi/autoinc.so %buildroot%_libdir/pgsql/test/regress
	cd  %buildroot%_libdir/pgsql/test/regress/
	strip *.so
	rm -f GNUmakefile Makefile
	cd -
	cp %SOURCE4 %buildroot%_libdir/pgsql/test/regress/Makefile
	chmod 0644 %buildroot%_libdir/pgsql/test/regress/Makefile
%endif

# Fix some more documentation
# gzip doc/internals.ps
cp %{SOURCE6} README.rpm-dist
cp %{SOURCE12} postgresql-9.0-A4.pdf
#mkdir -p %{buildroot}%{pgbaseinstdir}/share/doc/html
#mv doc/src/sgml/html %{buildroot}%{pgbaseinstdir}/share/doc/html
#mv %{buildroot}%{pgbaseinstdir}/share/doc/html doc
#mkdir -p %{buildroot}%{pgbaseinstdir}/share/man/
#mv doc/src/sgml/man1 doc/src/sgml/man3 doc/src/sgml/man7 %{buildroot}%{pgbaseinstdir}/share/man/
#rm -rf %{buildroot}%{_docdir}/pgsql

rm -f %buildroot%_libdir/*.so
rm -f %buildroot%_libdir/libpgtypes*
rm -f %buildroot%_libdir/libecpg*
rm -rf %buildroot%_includedir
rm -f %buildroot%_bindir/pg_config
rm -f %buildroot%_bindir/ecpg
rm -f %buildroot%_datadir/locale/*/*/pg_config*.mo
rm -f %buildroot%_datadir/locale/*/*/ecpg*.mo
rm -f %buildroot%_man1dir/ecpg*
rm -f %buildroot%_man1dir/pg_config*
rm -rf %buildroot%_libdir/pgsql/pgxs
rm -f %buildroot%_libdir/*.a

# our libicu is so.38 (see files contrib too), tr links to files during copying
%if %icu
	cp %_libdir/libicuuc.so.38 %buildroot/%_libdir/pgsql/
	cp %_libdir/libicudata.so.38 %buildroot/%_libdir/pgsql/
	cp %_libdir/libicui18n.so.38 %buildroot/%_libdir/pgsql/
%endif

%find_lang libpq5-9.0
%find_lang initdb-9.0
%find_lang pg_ctl-9.0
%find_lang pg_dump-9.0
%find_lang postgres-9.0
%find_lang psql-9.0
%find_lang pg_resetxlog-9.0
%find_lang pg_controldata-9.0
%find_lang pgscripts-9.0
%find_lang plperl-9.0
%find_lang plpgsql-9.0
%find_lang plpython-9.0

cat initdb-9.0.lang pg_ctl-9.0.lang psql-9.0.lang pg_dump-9.0.lang pgscripts-9.0.lang > main.lst
cat postgres-9.0.lang pg_resetxlog-9.0.lang pg_controldata-9.0.lang plpgsql-9.0.lang > server.lst

%if %_vendor == "alt"
%else
install -m644 %SOURCE17 %buildroot%_sysconfdir/sysconfig/postgresql

%post -n libpq5.2-9.0eter -p %post_ldconfig
if [ -f /etc/debian_version ]; then
   grep 'en_US en_US.UTF8' /etc/locale.alias || locale-gen en_US && dpkg-reconfigure locales
fi
%postun -n libpq5.2-9.0eter -p %postun_ldconfig
%endif

# FIXME: 26 uid!
%pre server
groupadd -g 26 -o postgres >/dev/null 2>&1 || :
useradd -g postgres -o -r -d /var/lib/pgsql -s /bin/bash \
	-c "PostgreSQL Server" -u 26 postgres >/dev/null 2>&1 || :
touch /var/log/pgsql
chown postgres:postgres /var/log/pgsql
chmod 0700 /var/log/pgsql

%post server
%if %_vendor == "alt"
%else
%post_ldconfig
%endif
%post_service postgresql

%preun server
%preun_service postgresql
%if %_vendor == "alt"
%else
%postun server
%post_ldconfig
%endif

%if %test
%post test
chown -R postgres:postgres %_datadir/pgsql/test >/dev/null 2>&1 || :
%endif

# FILES section.

%files -f main.lst
%doc doc/KNOWN_BUGS doc/MISSING_FEATURES doc/README*
%doc COPYRIGHT README HISTORY doc/bug.template
%doc README.rpm-dist
%_bindir/clusterdb
%_bindir/createdb
%_bindir/createlang
%_bindir/createuser
%_bindir/dropdb
%_bindir/droplang
%_bindir/dropuser
%_bindir/pg_dump
%_bindir/pg_dumpall
%_bindir/pg_restore
%_bindir/psql
%_bindir/reindexdb
%_bindir/vacuumdb
#%_man1dir/clusterdb.*
#%_man1dir/createdb.*
#%_man1dir/createlang.*
#%_man1dir/createuser.*
#%_man1dir/dropdb.*
#%_man1dir/droplang.*
#%_man1dir/dropuser.*
#%_man1dir/pg_dump.*
#%_man1dir/pg_dumpall.*
#%_man1dir/pg_restore.*
#%_man1dir/psql.*
#%_man1dir/reindexdb.*
#%_man1dir/vacuumdb.*
#%_man7dir/*

%files docs 
%doc doc/src/* 
%doc *-A4.pdf 
%doc src/tutorial 
%doc doc/src/sgml/html 
%_docdir/pgsql/contrib/README.fasttrun 
%_docdir/pgsql/contrib/README.fulleq 
%_docdir/pgsql/contrib/README.mchar 
%_docdir/pgsql/contrib/autoinc.example 
%_docdir/pgsql/contrib/insert_username.example 
%_docdir/pgsql/contrib/moddatetime.example 
%_docdir/pgsql/contrib/refint.example 
%_docdir/pgsql/contrib/timetravel.example


%files contrib
%_libdir/pgsql/mchar.so
%_libdir/pgsql/fulleq.so
%_libdir/pgsql/fasttrun.so
%_libdir/pgsql/_int.so
%_libdir/pgsql/autoinc.so
%_libdir/pgsql/btree_gist.so
%_libdir/pgsql/chkpass.so
%_libdir/pgsql/cube.so
%_libdir/pgsql/dblink.so
%_libdir/pgsql/earthdistance.so
%_libdir/pgsql/fuzzystrmatch.so
%_libdir/pgsql/insert_username.so
%_libdir/pgsql/isn.so
%_libdir/pgsql/hstore.so
%_libdir/pgsql/pg_freespacemap.so
%_libdir/pgsql/pgrowlocks.so
%if %ssl
%_libdir/pgsql/sslinfo.so
%endif
%_libdir/pgsql/lo.so
%_libdir/pgsql/ltree.so
%_libdir/pgsql/moddatetime.so
%_libdir/pgsql/pgcrypto.so
%_libdir/pgsql/pgstattuple.so
%_libdir/pgsql/pg_buffercache.so
%_libdir/pgsql/pg_trgm.so
%_libdir/pgsql/refint.so
%_libdir/pgsql/seg.so
%_libdir/pgsql/tablefunc.so
%_libdir/pgsql/timetravel.so
%_libdir/pgsql/tsearch2.so
%_libdir/pgsql/adminpack.so
%_libdir/pgsql/dict_int.so
%_libdir/pgsql/dict_xsyn.so
%_libdir/pgsql/euc2004_sjis2004.so
%_libdir/pgsql/pageinspect.so
%_libdir/pgsql/test_parser.so
%_libdir/pgsql/auto_explain.so
%_libdir/pgsql/btree_gin.so
%_libdir/pgsql/citext.so
%_libdir/pgsql/pg_stat_statements.so

%if %xml
%_libdir/pgsql/pgxml.so
%endif
%_datadir/pgsql/contrib/
%_datadir/pgsql/tsearch_data
%_bindir/oid2name
%_bindir/pgbench
%_bindir/vacuumlo
%if %icu
%_libdir/pgsql/libicuuc.so.38
%_libdir/pgsql/libicudata.so.38
%_libdir/pgsql/libicui18n.so.38
%endif
%doc contrib/*/README.* contrib/spi/*.example
%doc contrib/adminpack/*.sql
%doc contrib/btree_gin/*.sql
%doc contrib/btree_gist/*.sql
%doc contrib/chkpass/*.sql
%doc contrib/citext/*.sql
%doc contrib/cube/*.sql
%doc contrib/dblink/*.sql
%doc contrib/dict_int/*.sql
%doc contrib/dict_xsyn/*.sql
%doc contrib/earthdistance/*.sql
%doc contrib/fuzzystrmatch/*.sql
%doc contrib/hstore/*.sql
%doc contrib/intagg/*.sql
%doc contrib/intarray/*.sql
%doc contrib/isn/*.sql
%doc contrib/lo/*.sql
%doc contrib/ltree/*.sql
%doc contrib/pageinspect/*.sql
%doc contrib/pg_buffercache/*.sql
%doc contrib/pgcrypto/*.sql
%doc contrib/pg_freespacemap/*.sql
%doc contrib/pgrowlocks/*.sql
%doc contrib/pg_stat_statements/*.sql
%doc contrib/pgstattuple/*.sql
%doc contrib/pg_trgm/*.sql
%doc contrib/seg/*.sql
%doc contrib/spi/*.sql
%doc contrib/sslinfo/*.sql
%doc contrib/tablefunc/*.sql
%doc contrib/test_parser/*.sql
%doc contrib/tsearch2/*.sql
%doc contrib/unaccent/*.sql
%doc contrib/uuid-ossp/*.sql
%doc contrib/xml2/*.sql

%files seltaaddon
%_libdir/pgsql/seltapgaddon.so
%doc contrib/seltapgaddon/*.sql



%files -n libpq5.2-9.0eter -f libpq5-9.0.lang
%_libdir/libpq-9.0eter.so.*

%files server -f server.lst
%config %_initdir/postgresql
%if %_vendor == "alt"
%else
%config(noreplace) %_sysconfdir/sysconfig/postgresql
%_initdir/postgresql.outformat
%endif
%if %pam
%config(noreplace) %_sysconfdir/pam.d/postgresql
%endif
%_bindir/initdb
%_bindir/pg_controldata
%_bindir/pg_ctl
%_bindir/pg_resetxlog
%_bindir/postgres
%_bindir/postmaster
%_bindir/pg_standby
%_bindir/pg_archivecleanup
%_bindir/pg_upgrade
%_libdir/pgsql/libpqwalreceiver.so
%_libdir/pgsql/passwordcheck.so
%_libdir/pgsql/pg_upgrade_support.so
%_libdir/pgsql/plpython2.so
%_libdir/pgsql/unaccent.so

#%_man1dir/initdb.*
#%_man1dir/pg_controldata.*
#%_man1dir/pg_ctl.*
#%_man1dir/pg_resetxlog.*
#%_man1dir/postgres.*
#%_man1dir/postmaster.*
%_datadir/pgsql/postgres.bki
%_datadir/pgsql/postgres.description
%_datadir/pgsql/postgres.shdescription
%_datadir/pgsql/system_views.sql
%_datadir/pgsql/*.sample
%_datadir/pgsql/timezone/
%_datadir/pgsql/timezonesets/
%_libdir/pgsql/plpgsql.so
%_libdir/pgsql/dict_snowball.so
%dir %_libdir/pgsql
%dir %_datadir/pgsql
%dir /var/lib/pgsql
%attr (700,postgres,postgres) %dir /var/lib/pgsql/data
%dir /var/lib/pgsql/backups
/var/lib/pgsql/.bash_profile
%_libdir/pgsql/*_and_*.so
%_datadir/pgsql/conversion_create.sql
%_datadir/pgsql/information_schema.sql
%_datadir/pgsql/sql_features.txt
%_datadir/pgsql/snowball_create.sql

%if %plperl
%files plperl -f plperl-9.0.lang
%_libdir/pgsql/plperl.so
%endif

%if %pltcl
%files pltcl
%_libdir/pgsql/pltcl.so
%_bindir/pltcl_delmod
%_bindir/pltcl_listmod
%_bindir/pltcl_loadmod
%_datadir/pgsql/unknown.pltcl
%endif

%if %plpython
%files plpython -f plpython-9.0.lang
%_libdir/pgsql/plpython.so
%endif

%if %test
%files test
%_libdir/pgsql/test/*
%dir %_libdir/pgsql/test
%endif

%changelog
* Mon Aug 01 2011 Stas Korobeynikov <stas@etersoft.ru> 9.0.2-alt8
- add patch for windows from general place to create script
- remove two one make

* Fri Jul 29 2011 Stas Korobeynikov <stas@etersoft.ru> 9.0.2-alt7
- revert "Change libpg-eter version." Do not wok with Linux

* Fri Jul 29 2011 Stas Korobeynikov <stas@etersoft.ru> 9.0.2-alt6
- delete sufix

* Fri Jul 29 2011 Stas Korobeynikov <stas@etersoft.ru> 9.0.2-alt5
- change vwrsion at windiws build script

* Fri Jul 29 2011 Stas Korobeynikov <stas@etersoft.ru> 9.0.2-alt4
- change path for addiditional program for postgres 9.0

* Thu Jul 28 2011 Stas Korobeynikov <stas@etersoft.ru> 9.0.2-alt3
- add parameters _disable_ld_no_undefined for mandriva (default --no-undefined pamater)
- change libpg-eter version
- enable embedded libicu again:
- new build 9.0.2-alt2 (with rpmlog script)
- renew nsis windows scripts
- update patch for windows nsis settings

* Thu Jul 28 2011 Stas Korobeynikov <stas@etersoft.ru> 9.0.2-alt2
- add parameters _disable_ld_no_undefined for mandriva (default --no-undefined pamater)
- change libpg-eter version
- renew nsis windows scripts

* Sat Jun 25 2011 Vitaly Lipatov <lav@altlinux.ru> 9.0.2-alt1.3
- bashprofile modified (added locale generating for Ubuntu)
- new package - seltaaddon added
- patch changing NAMEDATALEN applying

* Thu Feb 3 2011 Alexander Shevelev <alexalv@altlinux.org> 9.0eter-alt0
- new version of postgresql

* Sun Oct 24 2010 Yuri Fil <yurifil@altlinux.org> 8.4.4-alt2
- add scripts for Windows rebuilds

* Tue Jul 06 2010 Yuri Fil <yurifil@altlinux.org> 8.4.4-alt1.1
- add configure --disable-integer-datetimes

* Thu Jun 24 2010 Yuri Fil <yurifil@altlinux.org> 8.4.4-alt1
- new version (8.4.4) import in git

* Wed Jun 16 2010 Yuri Fil <yurifil@altlinux.org> 8.4.3-alt2.1
- small fixes in spec for Ubuntu rebuild

* Fri Apr 16 2010 Boris Savelev <boris@altlinux.org> 8.4.3-alt2
- fix initdb in init-script -- do not use 'sameuser' auth type

* Mon Mar 15 2010 Boris Savelev <boris@altlinux.org> 8.4.3-alt1
- new version

* Sat Feb 13 2010 Boris Savelev <boris@altlinux.org> 8.4.2-alt1
- new version (8.4.2)
- add libpq subpackage
- clean spec

* Sat Nov 14 2009 Boris Savelev <boris@altlinux.org> 8.4.1-alt2
- small fixes

* Sat Nov 14 2009 Boris Savelev <boris@altlinux.org> 8.4.1-alt1
- new version (8.4.1)

* Sat Nov 14 2009 Boris Savelev <boris@altlinux.org> 8.3.8-alt2
- update Conflicts

* Sat Nov 14 2009 Boris Savelev <boris@altlinux.org> 8.3.8-alt1
- new version
- update 1c patchset

* Tue Oct 20 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt5
- replace patch for fix buffer overflow

* Thu Aug 06 2009 Vitaly Lipatov <lav@altlinux.ru> 8.3.7-alt4
- drop out obsoleted code from init script

* Wed Aug 05 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt3.3
- read additional conf from /etc/sysconfig/postgresql (closes: eterbug#4153, eterbug#4133)
- add /etc/sysconfig/postgresql for non-ALT
- add requires to posgresql-common for ALT

* Fri Jul 24 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt3.2
- read additional conf from /etc/sysconfig/postgresql

* Wed Jul 22 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt3.1
- realy fix buffer overflow

* Mon Jul 20 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt3
- fix build for buffer overflow

* Wed Jun 17 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt2.1
- fix CFLAGS for static libpq build (non-ALT systems)

* Sun Apr 05 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt2
- build with static libpq for non-ALT

* Thu Mar 19 2009 Boris Savelev <boris@altlinux.org> 8.3.7-alt1
- new version (8.3.7)

* Sat Feb 07 2009 Boris Savelev <boris@altlinux.org> 8.3.6-alt1
- new version (8.3.6)

* Fri Nov 07 2008 Boris Savelev <boris@altlinux.org> 8.3.5-alt1
- new version

* Mon Sep 22 2008 Boris Savelev <boris@altlinux.org> 8.3.3-alt1
- PostgreSQL 8.3
- new 1c patches

* Wed Aug 06 2008 Boris Savelev <boris@altlinux.org> 8.2.9-alt1
- new version
- split 1c patch and etersoft fix
- build with system libicu

* Tue Jun 21 2008 Boris Savelev <boris@altlinux.org> 8.2.4-alt1e11
- build with 1c patch to 1c_FULL_82-0.18.patch

* Tue Apr 22 2008 Boris Savelev <boris@altlinux.org> 8.2.4-alt1e10
- fix eterbug #1734

* Thu Mar 20 2008 Boris Savelev <boris@altlinux.ru> 8.2.4-alt1e9
- add postgresql.outformat
- rebuild with new rpm-build-compat (fix preinstall scripts)

* Sat Feb 23 2008 Vitaly Lipatov <lav@altlinux.ru> 8.2.4-alt1e7
- add requires on libicu38

* Thu Feb 14 2008 Vitaly Lipatov <lav@altlinux.ru> 8.2.4-alt1e6
- cleanup spec, use correct post/pre scripts

* Wed Feb 13 2008 Vitaly Lipatov <lav@altlinux.ru> 8.2.4-alt1e5
- Added some patches:
  timestamp.c.diff
  postgresql.conf.sample.patch
  pg_hba.conf.sample.patch
- Updated initscript

* Sun Dec 30 2007 Vitaly Lipatov <lav@altlinux.ru> 8.2.4-alt1e4
- provides libicu from contrib package (disable requires for external libicu)

* Tue Dec 18 2007 Vitaly Lipatov <lav@altlinux.ru> 8.2.4-alt1e3
- incorporate libicu in the contrib package
- enable SMP build
- fix mvarchar_in (remove typmod param)
- replace pushd/popd with cd / cd -
- disable pltcl subpackage

* Thu Oct 18 2007 Vitaly Lipatov <lav@altlinux.ru> 8.2.4-alt1e2
- cleanup spec with rpmcs

* Fri Sep 28 2007 Sergey Alembekov <rt@altlinux.ru> 8.2.4-alt1
- Cleaning spec for build with ALTLinux 4.0
- Update to 8.2.4
- Add conflicts with postgres8.2
- Where is bug in upstream: ACCESS EXCLUSIVE mode doesn't work

* Wed Feb 7 2007 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.3-1PGDG
- Update to 8.2.3

* Tue Feb 6 2007 Sander Steffann <s.steffann@computel.nl> 8.2.2-2PGDG
- Replaced %kerbdir/%_libdir with %kerbdir/%_lib. The former
  directory can never be valid.

* Sat Feb 3 2007 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.2-1PGDG
- Update to 8.2.2

* Fri Jan 12 2007 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.1-2PGDG
- Split -pl subpackage into three new packages.

* Wed Jan 3 2007 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.1-1PGDG
- Update to 8.2.1
- Fix 64-bit build problem when pgfts is enabled

* Sat Dec 16 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.0-4PGDG
- Enable thread safety to be compatible with RH/FC RPMs.

* Sun Dec 10 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.0-3PGDG
- Fix BuildRequires for tcl package, per pg #2813
- Add 8.2-A4 PDF again

* Tue Dec 5 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.0-2PGDG
- We are not in beta; set beta to 0

* Sat Dec 2 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2.0-1PGDG
- Update to 8.2.0

* Sun Nov 26 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2rc1-1PGDG
- Update to RC1

* Sun Nov 12 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2beta3-2PGDG
- Added pl/python related parts again.

* Sun Nov 12 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2beta3-1PGDG
- Update to beta3
- Updated installed file list per changes in 8.2
- Temporarily disable pdf form of the docs until we build 8.2 pdf.
- Cosmetic cleanup (also surpress rpmlint warnings about this)

* Mon Jul 10 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.2beta-1PGDG
- Pushed html docs into -docs RPM
- Removed JDBC portions and splitted it into seperate RPM (postgresql-jdbc)
- Removed PyGreSQL portions and splitted it into seperate RPM (postgresql-python)
- Removed tcl portions and splitted it into seperate RPM (postgresql-tcl)
- Initial spec for 8.2.0

* Tue Jun 06 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.4-3PGDG
- Updated PyGreSQL from 3.8 to 3.8.1

* Sat May 27 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.4-2PGDG
- Fix tcl builds, patch from Tom Lane.
- Update JDBC jars to build 407

* Mon May 22 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.4-1PGDG
- Temporarily disable tcl builds, until they or we fix the problem.
- Update JDBC jars to build 406
- Fix PyGreSQL filenames
- Update to 8.1.4

* Sun Feb 19 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.3-2PGDG
- Updated PyGreSQL from 3.7 to 3.8

* Mon Feb 13 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.3-1PGDG
- Update to 8.1.3

* Thu Feb 09 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.2-2PGDG
- Update JDBC jars to build 405

* Tue Jan 10 2006 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.2-1PGDG
- Update to  8.1.2

* Sat Dec 10 2005 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.1-2PGDG
- Re-added PDF documentation into RPM.

* Sat Dec 10 2005 Devrim GUNDUZ <devrim@commandprompt.com> 8.1.1-1PGDG
- 8.1.1

* Thu Dec 5 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1.0-5PGDG
- Enabled tcl builds by default.

* Thu Dec 01 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1.0-4PGDG
- Added a macro for RHEL 3 kerberos builds.

* Sun Nov 27 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1.0-3PGDG
- re-added aconfver macro definition

* Sun Nov 06 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1.0-2PGDG
- Update JDBC jars to stable version

* Sat Nov 05 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1.0-1PGDG
- Update to 8.1.0
- Updated JDBC jars
- Updated README.rpm-dist for 8.1
- added man1/reindexdb to files list.

* Sun Oct 30 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1RC1-1PGDG
- Update to 8.1RC1

* Thu Oct 19 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1beta4-1PGDG
- Update to 8.1beta4

* Thu Oct 19 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1beta3-2PGDG
- Fixed postgresql.pam file
- Fixed bug #167040, part 2

* Thu Oct 13 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1beta3-1PGDG
- Update to 8.1beta3
- Updated JDBC jars
- Updated rpm-pgsql.patch
- Include contrib/xml2 in build (bug #167492, Tom Lane)
- Add %_sysconfdir/pam.d/postgresql (bug #167040 , Tom Lane)
- Add rpath to plperl.so (bug #162198 , Tom Lane)
- Adjust pgtcl link command to ensure it binds to correct libpq (bug #166665 , Tom Lane)

* Sun Sep 18 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1beta2-1PGDG
- Update to 8.1beta2

* Thu Sep 08 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1beta1-6PGDG
- Updated PyGreSQL from 3.6.2 to 3.7

* Sun Aug 28 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.1beta1-1..5PGDG
- Update to 8.1beta1
- Updated JDBC jars
- Updated postgresql-logging.patch
- Removed deprecated contrib entries
- Added old contrib entries that are moved to the core.
- Removed Python conflict check (bz#166754)
- Removed autoconf call

* Fri May 6 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.0.3-1PGDG
- Update to 8.0.3-1

* Tue Apr 19 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.0.2-2PGDG
- Update to 8.0.2-2
- Updated JDBC jars
- Updated PyGreSQL from 3.6.1 to 3.6.2.

* Fri Apr 08 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.0.2-1PGDG
- Update to 8.0.2

* Mon Mar 28 2005 Devrim GUNDUZ <devrim@PostgreSQL.org> 8.0.2beta1-1PGDG
- Update to 8.0.2beta1
- Updated PyGreSQL from 3.6.1 to 3.6.2

* Wed Feb 23 2005 Devrim GUNDUZ <devrim@gunduz.org> 8.0.1-2PGDG
- Repair improper error message in init script when PGVERSION doesn't match. (Tom Lane)
- Arrange for auto update of version embedded in init script. (Tom Lane)
- Update JDBC jars

* Mon Jan 31 2005 Devrim GUNDUZ <devrim@gunduz.org> 8.0.1-1PGDG
- Added dependency for e2fsprogs-devel for krb5 builds (David Zambonini)
- Fix tcl build problems on Red Hat 8.0 (David Zambonini)
- Fixed krb5 builds (David Zambonini)
- Update to 8.0.1-1

* Tue Jan 18 2005 Devrim GUNDUZ <devrim@gunduz.org> 8.0.0-1PGDG
- Update to 8.0.0-1
- Updated JDBC Jars

* Sun Jan 16 2005 Devrim GUNDUZ <devrim@gunduz.org> 8.0.0rc5-4PGDG
- rc5
- Updated PyGreSQL from 3.6 to 3.6.1
- pgtcl 1.5.2 (per Tom Lane)
- Regression tests are run during RPM build (disabled by default) (Tom Lane)
- Postmaster stderr goes someplace useful, not /dev/null (bz#76503, #103767) (Tom Lane)
- Make init script return a useful exit status (bz#80782) (Tom Lane)
- Move docs' tutorial directory to %%{_libdir}/pgsql/tutorial, since it includes .so files that surely do not belong under %_datadir. (Tom Lane)

* Tue Jan 11 2005 Devrim GUNDUZ <devrim@gunduz.org> 8.0.0rc5-2PGDG
- pre-rc5
- Updated PyGreSQL from 3.5 to 3.6

* Fri Dec 03 2004 Devrim GUNDUZ <devrim@gunduz.org> 8.0.0rc1-2PGDG
- rc1-2PGDG
- Tag for PGDG
- Updated kerbdir
- Updated PyGreSQL from 3.4 to 3.5
- Updated doc files for PyGreSQL
- Modified if-endif lines for tcl&tcldevel prereq lines (per Red Hat RPMS)
- Applied getppid.patch as patch #4 (per Red Hat RPMS)
- Updated spec file to correct permissions for PyGreSQL permissions (per Red Hat RPMS)
- Updated preun and postun server scripts, per Red Hat RPMS

* Fri Dec 03 2004 Joe Conway <mail@joeconway.com> 8.0.0rc1-1JEC
- Updated for rc1
- Updated jdbc to latest beta (pg80b1.308*)

* Tue Nov 30 2004 Joe Conway <mail@joeconway.com> 8.0.0beta5-2JEC
- Fix init script to reflect version 8.0

* Mon Nov 22 2004 Joe Conway <mail@joeconway.com> 8.0.0beta5-1JEC
- Updated/adjusted for beta5
- Renamed to 8.0.0beta5-1JEC to avoid confusion with PGDG official releases

* Wed Aug 18 2004 Joe Conway <mail@joeconway.com> 8.0.0beta1-4PGDG
- Added back jdbc, per disscussion on HACKERS mailing list

* Tue Aug 17 2004 Joe Conway <mail@joeconway.com> 8.0.0beta1-3PGDG
- Fix init script; check for version 8.0, not 7.4

* Mon Aug 16 2004 Joe Conway <mail@joeconway.com> 8.0.0beta1-2PGDG
- Install missing file needed by initdb

* Sat Aug 14 2004 Joe Conway <mail@joeconway.com>
- 8.0.0beta1-1PGDG
- Update for 8.0.0beta1
- removed jdbc and tcl options
